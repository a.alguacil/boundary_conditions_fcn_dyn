param = {
    'randomSeed' : 0,
    'geometry': {
        "lx": "100.",
        "ly": "100.",   
    },
    'acoustics': {
        "rho0": "1.0",
        "gaussianPulse": {
            "userDefined": {
                "numUserPulses": "1",
                "center": {
                    "x": "20.0",
                    "y": "31.0",
                },
                "amplitude": "0.001",
                "halfWidth": "6",
            },
            "randomDefined": {
                "numRandPulses": "0 0",
                "center": {
                    "x": "0 0",
                    "y": "0 0"
                },
                "amplitude": "0 0",
                "halfWidth": "0 0",    
            }
  
        },
    },
    "numerics" : {
        'nu' : "0.0",
        'resolution' : "200",
    },
    'boundaryConditions': {
        "type" : {
            "left" : "16",
            "right" : "16",
            "bottom" : "16",
            "top" : "16",
        },
        "spongeZones": {
            "xWidths" : "0.0 0.0",
            "yWidths" : "0.0 0.0",
        },
    },
    "output": {
        "outputDir": "temp/",
        "maxT" : "6",
        "vtkIter" : "1",
        "numSim" : "1",
    }
}
