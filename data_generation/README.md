## Dataset generation

This folder contains a C++ script to generate the 2D acoustic pulses databases.
It relies on the open-source Lattice-Boltzmann Fluids Mechanics library (University of Geneva).
Install it in this folder using the following command:

```
git clone git@gitlab.com:unigespc/palabos.git
```


Compiling the code 
------------

On a UNIX system, execute the following commands to compile the solver :

```
mkdir build/
cd build/
cmake ..
make
```

Generating the databases
------------

## Training and validation

For performing the simulations, employ the following command:

```
mpirun -np <NTASKS> generate_dataset <paramXML>
```

where `NTASKS` is the number of MPI processes used for the simulations and `paramXML` is an XML file with the simulation properties.
One can create a custom parameters input file.

The database used in the article can be reproduced by using the xml files in the three following folder:

+ `d1_reflecting` - 2D simulations with Gaussian Pulses as initial condition and reflecting boundary conditions.
+ `d2_periodic` - 2D simulations with Gaussian Pulses as initial condition and periodic boundary conditions.
+ `d3_absorbing` - 2D simulations with Gaussian Pulses as initial condition and absorbing boundary conditions.

For each folder, three subfolders are present:

+ `training` and `validation`, splitting the generation of train and val data. Follow this command:

```
mpirun -np <NTASKS> ../../generate_dataset <paramXML>
```
+ `testing`, employing the same RNG seed to generate 25 initial conditions on the three studied BCs for testing the trained NNs. Generate it using this command:
```
python3 generate_xml.py
./launch_generation.sh
```

Results will be written in `root_folder/datasets` by default.

Dependencies (tested version)
------------

- GCC (8.2.0)
- OpenMPI (4.0.0)
- CMake (3.13.2)
- Palabos (2.2.1)
- Python (3.6)
- Python libraries:
    - Numpy (1.19)
    - dicttoXML (1.7.4)
    
