#!/bin/bash

for TASK_ID in {1..25}
do
    echo "Starting task $TASK_ID"
    xmlName=$(sed -n "${TASK_ID}p" files.txt)
    mpirun -np 2 ../../generate_dataset $xmlName
done

