#!/usr/bin/env python3
# -*- coding: utf-8 -*-
r"""

Automatic generation of xml files for test data generation (periodic case)
To run:
    python3 generate_xml
"""

# native libraries
import argparse
import copy
import os

# third-party libraries
import numpy as np
from dicttoxml import dicttoxml
from xml.dom.minidom import parseString

# local libraries
import param_dict

np.random.seed(100)

def writeXML(dictionary, outname):
    xml_snippet = dicttoxml(dictionary, custom_root='param', attr_type=False)
    dom = parseString(xml_snippet)
    with open(outname, "w") as f:
        f.write(dom.toprettyxml())

def to_string_list(var):
    return str(var).replace("[", "").replace("]","").replace(",","")

# simulation params 
D0 = 100
res0 = 200
dx0 = D0 / (res0)
vtkIter0 = 1
cs = 1/3**0.5
nIter = 600
b = 6           #pulse half-width
eps = 0.001     #pulse initial amplitude

# Total number of simulations
Num_sims = 25

# periodic BC case
left_bc = 64
right_bc = 64
bottom_bc = 64
top_bc = 64
sponge_width = 0.

# output folder
root_folder = '../../../datasets/d2/testing'

# Initial Conditions
num_pulses = np.array([1])
num_pulses = np.append(num_pulses, np.random.randint(1, 5, Num_sims - 1))

# First one is centered pulse
c_x = [np.array([50.])]
c_y = [np.array([50.])]

# Rest are randomly sampled
for n in num_pulses[1:]:
    print(n)
    c_x.append(np.random.uniform(20, 80, n))
    c_y.append(np.random.uniform(20, 80, n))

dir_file = open("files.txt", "w")
info_file = open("info.txt", "w")

idx = 0
info_file.write("{:>7}, {:>7}, {:>7}, {:>7}, {:>7}, {:>7}\n"
                .format("id", "b/D", "b/dx", "b", "D", "res", "nIter", "maxTau"))

for n in num_pulses:
    dx=dx0
    res = int(D0 / dx)
    D = D0
    param_temp = copy.deepcopy(param_dict.param)
    tau = nIter * cs / res #non-dimensional time
    param_temp['geometry']['lx'] = str(D)
    param_temp['geometry']['ly'] = str(D)


    param_temp['acoustics']['gaussianPulse']['userDefined']['numUserPulses'] = str(n)
    
    param_temp['acoustics']['gaussianPulse']['userDefined']['center']['x'] = to_string_list(c_x[idx])
    param_temp['acoustics']['gaussianPulse']['userDefined']['center']['y'] = to_string_list(c_y[idx])

    param_temp['acoustics']['gaussianPulse']['userDefined']['amplitude'] = to_string_list([eps]*n)
    param_temp['acoustics']['gaussianPulse']['userDefined']['halfWidth'] = to_string_list([b]*n)
    
    param_temp['numerics']['resolution'] = str(res)
    
    param_temp['boundaryConditions']['type']['left'] = str(left_bc)
    param_temp['boundaryConditions']['type']['right'] = str(right_bc)
    param_temp['boundaryConditions']['type']['bottom'] = str(bottom_bc)
    param_temp['boundaryConditions']['type']['top'] = str(top_bc)
    
    param_temp['boundaryConditions']['spongeZones']['xWidths'] = to_string_list([sponge_width]*2)
    param_temp['boundaryConditions']['spongeZones']['yWidths'] = to_string_list([sponge_width]*2)

    param_temp['output']['outputDir'] = os.path.abspath(os.path.join(root_folder, "{:03d}/".format(idx)))
    param_temp['output']['maxT'] = nIter
    param_temp['output']['vtkIter'] = vtkIter0

    outdir = os.path.join(root_folder, "{:03d}/".format(idx))
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    outname = outdir + 'param_d1_id{:03d}.xml'.format(idx,n)
    dir_file.write(f"{os.path.abspath(outname)}\n")
    info_file.write("{:7d}, {:7.2f}, {:7.2f}, {:7.2f}, {:7.2f}, {:7.2f}\n"
            .format(idx, b, D, res, nIter//vtkIter0, tau))

    writeXML(param_temp, outname)
    idx = idx + 1

info_file.write("Number of cases = {}".format(idx))
info_file.close()
dir_file.close()
