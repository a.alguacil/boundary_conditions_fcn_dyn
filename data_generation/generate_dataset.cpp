/*
    2D simulations of acoustic pulses with varying boundary conditions using a
    Lattice Boltzmann approach.
    Results are saved in the .vti format, readable with Paraview software
    or with Python vtk library.
    Input parameters are set in the param.xml file and usage of the code is:
    Sequential:
        ./generate_dataset param.xml
    or in parallel, using MPI:
        mpirun -np <N_MPI_TASKS> generate_dataset param.xml
*/
#include <vector>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <random>
#include <sys/stat.h> // To create output folder.

#include "palabos2D.h"
#include "palabos2D.hh"


using namespace plb;
using namespace std;

typedef double T;
#define DESCRIPTOR plb::descriptors::AbsorbingWaveD2Q9Descriptor


// 

struct Param
// Parameters of the simulation
{
    /*
     * User defined parameters.
     */

    // Random numbers generator
    plint randomSeed;
    
    // Geometry.
    T lx;                                            // Size of computational domain, in physical units.
    T ly;                                            // Size of computational domain, in physical units.
    
    // Pressure field
    T rho0;                                          // Density offset.

    // Gaussian pulses (Defined by user).
    plint numUserPulses;                             // Number user-defined pulses. 
    std::vector<T> userCx;                           // Position of the pulse center, in physical units.
    std::vector<T> userCy;                           // Position of the pulse center, in physical units.
    std::vector<T> userEps;                          // Gaussian pulse amplitude with respect to offset. 
    std::vector<T> userB;                            // Gaussian pulse half-width, in physical units. 
    
    // Randomly generated gaussian pulses.
    Array<plint, 2> statNumRandPulses;               // Number of randomly generated pulses. 
    Array<T,2> statCx;                               // Position of the pulse center, in physical units.
    Array<T,2> statCy;                               // Position of the pulse center, in physical units.
    Array<T,2> statEps;                              // Gaussian pulse amplitude with respect to offset. 
    Array<T,2> statB;                                // Gaussian pulse half-width, in physical units. 

    // Numerics.
    T nu;                                            // Kinematic viscosity, in physical units.
    plint resolution;                                // Number of lattice nodes along a reference length.

    // Boundary Condtions.
    plint randBCType;                                // randomly chose between BCs
                                                     // 0: false, 1: abs/refleting, 2:abs/reflecting/periodic
    Array<plint,4> bcType;                           // BC Type
    Array<T,4> spongeWidths;                         // Sponge zone widths
                                                     // (0.0 for no sponge zone for the corresponding
                                                     // lattice boundary).
                                                     // If type == reflecting, no effect.
    // Output
    T maxT;                                          // Time for events in lattice units.

    /*
     * Derived parameters
     */
    T dx;                                            // Discrete space step.
    T dt;

    Array<T,2> physicalLocation;

    plint nx;                                        // Grid resolution of bounding box.
    plint nx_domain;                                 // Grid resolution of physcial domain (excluding sponge zones).
    plint ny;                                        // Grid resolution of bounding box.
    plint ny_domain;                                 // Grid resolution of physcial domain (excluding sponge zones).

    Box2D physdomain;                                // box defining the physical domain (excluding sponges)
    Box2D fulldomain;                                // box defining the whole domain (including
                                                     // sponge zones).

    T nuLB;                                          // Kinematic viscosity, in lattice units.
    T omega;                                         // LB relaxation parameter.
    Array<T,2> uLB;                                  // Velocity in lattice units (numerical parameters).
    T uLB_norm;                                      // Norm of velocity in lattice units (numerical parameters).
    
    // Pulses
    plint numRandPulses;
    plint totalNumPulses;
    std::vector<T> cx;                               // Position of the pulse center, in physical units.
    std::vector<T> cy;                               // Position of the pulse center, in physical units.
    std::vector<T> eps;                              // Gaussian pulse amplitude with respect to offset. 
    std::vector<T> b;                                // Gaussian pulse half-width, in physical units. 
    
    std::vector<T> cxLB;                             // Position of the pulse center, in LB units.
    std::vector<T> cyLB;                             // Position of the pulse center, in LB units.
    
    // Misc
    std::string outputDir;                           // Output Directory
    plint maxIter;                                   // Time for events in lattice units.
    plint vtkIter;                                   // Time for vtk output.
    plint numSim;                                    // Number of simulations.

    Param()
    { }

    Param(std::string xmlFname)
    {
        XMLreader document(xmlFname);
	    document["param"]["randomSeed"].read(randomSeed);
        
	    document["param"]["geometry"]["lx"].read(lx);
        document["param"]["geometry"]["ly"].read(ly);
        physicalLocation = {0.0, 0.0};

        document["param"]["acoustics"]["rho0"].read(rho0);

        document["param"]["acoustics"]["gaussianPulse"]["userDefined"]["numUserPulses"].read(numUserPulses);
        PLB_ASSERT(numUserPulses >= 0);

        
        if (numUserPulses > 0) 
        {
            std::vector<T> x, y, k, l;
            document["param"]["acoustics"]["gaussianPulse"]["userDefined"]["center"]["x"].read(x);
            document["param"]["acoustics"]["gaussianPulse"]["userDefined"]["center"]["y"].read(y);
            document["param"]["acoustics"]["gaussianPulse"]["userDefined"]["amplitude"].read(k);
            document["param"]["acoustics"]["gaussianPulse"]["userDefined"]["halfWidth"].read(l);
            PLB_ASSERT(x.size() == numUserPulses && y.size() == numUserPulses);
            PLB_ASSERT(k.size() == numUserPulses && l.size() == numUserPulses);
            userCx.resize(numUserPulses);
            userCy.resize(numUserPulses);
            userEps.resize(numUserPulses);
            userB.resize(numUserPulses);
            for (plint i = 0; i < numUserPulses; i++) {
                userCx[i] = x[i];
                userCy[i] = y[i];
                userEps[i] = k[i];
                userB[i] = l[i];
            }
        }
        
        document["param"]["numerics"]["nu"].read(nu);
        document["param"]["numerics"]["resolution"].read(resolution);

        {
            std::vector<plint> i;
            std::vector<T> u, v, x, y;
            document["param"]["acoustics"]["gaussianPulse"]["randomDefined"]["numRandPulses"].read(i);
            document["param"]["acoustics"]["gaussianPulse"]["randomDefined"]["center"]["x"].read(u);
            document["param"]["acoustics"]["gaussianPulse"]["randomDefined"]["center"]["y"].read(v);
            document["param"]["acoustics"]["gaussianPulse"]["randomDefined"]["amplitude"].read(x);
            document["param"]["acoustics"]["gaussianPulse"]["randomDefined"]["halfWidth"].read(y);

            for (plint j = 0; j < 2; j++) {
                statNumRandPulses[j] = i[j];
                statCx[j] = u[j];
                statCy[j] = v[j];
                statEps[j] = x[j];
                statB[j] = y[j];

            }
        }
        
        if (randBCType == 0)
        {
            plint left, right, bottom, top;
            document["param"]["boundaryConditions"]["type"]["left"].read(left);
            document["param"]["boundaryConditions"]["type"]["right"].read(right);
            document["param"]["boundaryConditions"]["type"]["bottom"].read(bottom);
            document["param"]["boundaryConditions"]["type"]["top"].read(top);

            bcType[0] = left;
            bcType[1] = right;
            bcType[2] = bottom;
            bcType[3] = top;
        }

        std::vector<T> zoneWidths;
        document["param"]["boundaryConditions"]["spongeZones"]["xWidths"].read(zoneWidths);
        PLB_ASSERT(zoneWidths.size() == 2);
        // left
        spongeWidths[0] = zoneWidths[0];
        // right
        spongeWidths[1] = zoneWidths[1];
        zoneWidths.clear();
        
        document["param"]["boundaryConditions"]["spongeZones"]["yWidths"].read(zoneWidths);
        PLB_ASSERT(zoneWidths.size() == 2);
        // bottom
        spongeWidths[2] = zoneWidths[0];
        // top
        spongeWidths[3] = zoneWidths[1];
        zoneWidths.clear();

        document["param"]["output"]["outputDir"].read(outputDir);
        document["param"]["output"]["maxT"].read(maxT);
        document["param"]["output"]["vtkIter"].read(vtkIter);
        document["param"]["output"]["numSim"].read(numSim);

    }

    void setSpongeWidths(T x0, T x1, T y0, T y1)
    {
        spongeWidths[0] = x0;
        spongeWidths[1] = x1;
        spongeWidths[2] = y0; 
        spongeWidths[3] = y1; 
    }
};

// Helper functions
int mkpath(const char* file_path, mode_t mode) {
  assert(file_path && *file_path);
  char* p;
  char* fp;
  fp = const_cast<char*>(file_path);
  for (p=strchr(fp+1, '/'); p; p=strchr(p+1, '/')) {
    *p='\0';
    if (mkdir(fp, mode)==-1) {
      if (errno!=EEXIST) {
          *p='/'; return -1;
      }
    }
    *p='/';
  }
  return 0;
}

enum CellType {
    TypeFluid = 1,
    TypeObstacle = 2,
    TypeVelocityDirichlet = 4,
    TypePressureNeumann = 8,
    TypeFreeslip = 16,
    TypeAbsorbing = 32,
    TypePeriodic = 64
};

bool isFluid(plint& cell) {return (plint) cell & (plint) TypeFluid;}
bool isObstacle(plint& cell) {return (plint) cell & (plint) TypeObstacle;}
bool isVelocityDirichlet(plint& cell) {return (plint) cell & (plint) TypeVelocityDirichlet;}
bool isPressureNeumann(plint& cell) {return (plint) cell & (plint) TypePressureNeumann;}
bool isFreeslip(plint& cell) {return (plint) cell & (plint) TypeFreeslip;}
bool isAbsorbing(plint& cell) {return (plint) cell & (plint) TypeAbsorbing;}
bool isPeriodic(plint& cell) {return (plint) cell & (plint) TypePeriodic;}

// conversion from LB to physical units
T toPhys(T lbVal, plint direction, T dx, Array<T,2> const& location)
{
    PLB_ASSERT(direction >= 0 && direction <= 1);
    return(lbVal * dx + location[direction]);
}

Array<T,2> toPhys(Array<T,2> const& lbVal, T dx, Array<T,2> const& location)
{
    return(lbVal * dx + location);
}

// conversion from physical to LB units
T toLB(T physVal, plint direction, T dx, Array<T,2> const& location)
{
    PLB_ASSERT(direction >= 0 && direction <= 1);
    return((physVal - location[direction]) / dx);
}

Array<T,2> toLB(Array<T,2> const& physVal, T dx, Array<T,2> const& location)
{
    return((physVal - location) / dx);
}

// End of helper functions

// Global variables definition
void computeDerivedParameters(Param& param)
{
    param.dx = param.ly / (param.resolution);                     // Discrete space step.
    param.dt = (T) 1.0;
    
    param.nuLB = param.nu * param.dt/(param.dx*param.dx);        // Kinematic viscosity, in lattice units.
    param.omega = 1.0/(DESCRIPTOR<T>::invCs2*param.nuLB+0.5);    // Relaxation parameter.

    // physical domain
    param.nx = util::roundToInt(param.lx/param.dx);              // Grid resolution of bounding box.
    // complete computational domain (adding PML layers)
    param.nx_domain = param.nx;
    if ( isAbsorbing(param.bcType[0]) ) {
        param.nx += util::roundToInt(param.spongeWidths[0]/param.dx);
    }
    if ( isAbsorbing(param.bcType[1]) ) {
        param.nx += util::roundToInt(param.spongeWidths[1]/param.dx);
    }
    param.ny = util::roundToInt(param.ly/param.dx);               // Grid resolution of bounding box.
    param.ny_domain = param.ny;
    if ( isAbsorbing(param.bcType[2]) ) {
        param.ny += util::roundToInt(param.spongeWidths[2]/param.dx);
    }
    if ( isAbsorbing(param.bcType[3]) ) {
        param.ny += util::roundToInt(param.spongeWidths[3]/param.dx);
    }

    // pulses in LB units
    param.cxLB.resize(param.totalNumPulses);
    param.cyLB.resize(param.totalNumPulses);
    for (plint i = 0; i < param.totalNumPulses; i++) {
        param.cxLB[i] = toLB(param.cx[i], 0,
                param.dx, param.physicalLocation);
        param.cyLB[i] = toLB(param.cy[i], 1,
                param.dx, param.physicalLocation);
    }    

    param.maxIter = util::roundToInt(param.maxT/param.dt);       // Time for events in lattice units.
}

void printParam(Param& param)
{

    pcout << "User defined parameters: " <<std::endl;
    pcout << "lx = " << param.lx << std::endl;
    pcout << "ly = " << param.ly << std::endl << std::endl;

    pcout << "INITIAL CONDITIONS" << std::endl;
    
    pcout << "Number of acoustic pulses: " << param.totalNumPulses << std::endl << std::endl;
    for (plint i = 0; i < param.totalNumPulses; i++) {
        pcout << "Pulse (" << i << "): " << std::endl;
        pcout << "cx = " << param.cx[i] << std::endl;
        pcout << "cy = " << param.cy[i]<< std::endl;
        pcout << "eps = " << param.eps[i] << std::endl;
        pcout << "b = " << param.b[i] << std::endl << std::endl;
    }

    pcout << "nu = " << param.nu << std::endl;
    pcout << "uLB = " << param.uLB_norm << std::endl;
    pcout << "res = " << param.resolution << std::endl;
    pcout << std::endl;
    
    std::vector<string> wallName = {"left", "right", "bottom", "top"};
    for (plint iZone = 0; iZone < 4; iZone++) {
        pcout << "boundary (" << wallName[iZone] << ") type = "
            << (CellType) param.bcType[iZone] << std::endl;
    }
    pcout << std::endl;

    for (plint iZone = 0; iZone < 4; iZone++) {
        if ( isAbsorbing(param.bcType[iZone]) ) {
            pcout << "spongeWidths[" << wallName[iZone] << "] = " << param.spongeWidths[iZone] << std::endl;
        }
        else {
            pcout << "spongeWidths[" << wallName[iZone] << "] = 0" << std::endl;
        }
    }
    pcout << std::endl;
    
    pcout << "maxT = " << param.maxT << std::endl << std::endl;
    
    pcout << "Derived parameters: " << std::endl;
    pcout << "dx = " << param.dx << std::endl;
    pcout << "dt = " << param.dt << std::endl;
    pcout << "nuLB = " << param.nuLB << std::endl;
    pcout << "omega = " << param.omega << std::endl;
    
    pcout << "Physical Location = [" << param.physicalLocation[0] 
        << ", " << param.physicalLocation[1] << "] " << std::endl;
    pcout << "nx = " << param.nx << std::endl;
    pcout << "ny = " << param.ny << std::endl;
    pcout << "nx (physical domain) = " << param.nx_domain << std::endl;
    pcout << "ny (physical domain) = " << param.ny_domain << std::endl;

    pcout << std::endl;
    pcout << "Output directory = " << param.outputDir << std::endl;
    pcout << std::endl;
}

void computeDomainGeometry(Param& param)
{
    // Initially LB origin is at (0,0) position in phys-units.
    // If a sponge zone is located at the bottom or left border,
    // the LB origin of the COMPUTATIONAL DOMAIN is moved resp. down or left.
    // The origin of the PHYSICAL DOMAIN (outside absorbing layers)
    // is ALWAYS at (0,0) in phys units.
    // This is to compute its position in LB units

    param.physicalLocation[0] = 0.0;
    param.physicalLocation[1] = 0.0;

    // left
    if ( isAbsorbing(param.bcType[0]) ) {
        param.physicalLocation[0] -= param.spongeWidths[0];
    }
    // bottom
    if ( isAbsorbing(param.bcType[2]) ) {
        param.physicalLocation[1] -= param.spongeWidths[2];
    }
    
    for (plint i = 0; i < 2; i++) {
        global::mpi().bCast(&param.physicalLocation[i], 1);
    }

}

void setRandomPulses(Param& param, std::mt19937& gen,
        std::uniform_int_distribution<plint>& distNum,
        std::uniform_real_distribution<T>& distCx,
        std::uniform_real_distribution<T>& distCy,
        std::uniform_real_distribution<T>& distEps,
        std::uniform_real_distribution<T>& distB )
{
    // Randomly selects pulse position (cx, cy), amplitude (eps) and half-width (b)
    param.numRandPulses = distNum(gen);
    param.totalNumPulses = param.numRandPulses + param.numUserPulses;

    global::mpi().bCast(&param.numRandPulses, 1);
    global::mpi().bCast(&param.totalNumPulses, 1);

    std::vector<T> tmpCx(param.numRandPulses);
    std::vector<T> tmpCy(param.numRandPulses);
    std::vector<T> tmpEps(param.numRandPulses);
    std::vector<T> tmpB(param.numRandPulses);

    param.cx = param.userCx;
    param.cy = param.userCy;
    param.eps = param.userEps;
    param.b = param.userB;

        if (global::mpi().isMainProcessor()) {
        for (plint i = 0; i < param.numRandPulses; i++) {
            tmpCx[i] = distCx(gen);
            tmpCy[i] = distCy(gen);
            tmpEps[i] = distEps(gen);
            tmpB[i] = distB(gen);
        }
    }

    // needed to get the same randomly generated number across MPI procs
    for (plint i = param.numUserPulses; i < param.totalNumPulses; i++) {
        global::mpi().bCast(&tmpCx[i], 1);
        global::mpi().bCast(&tmpCy[i], 1);
        global::mpi().bCast(&tmpEps[i],1);
        global::mpi().bCast(&tmpB[i],  1);
    }
    
    if (param.numRandPulses > 0){
    	param.cx.insert(std::end(param.cx), std::begin(tmpCx), std::end(tmpCx));
    	param.cy.insert(std::end(param.cy), std::begin(tmpCy), std::end(tmpCy));
    	param.eps.insert(std::end(param.eps), std::begin(tmpEps), std::end(tmpEps));
    	param.b.insert(std::end(param.b), std::begin(tmpB), std::end(tmpB));
    }
}

// global variable, accessible to all the functions defined afterwards
Param param;

// Locate voxels with absorbing BC and create a Boolean mask
template<typename T>
class AbsorbingBC2D : public DomainFunctional2D {
public:
    AbsorbingBC2D()
    { 
        x0 = toLB((T) 0, 0, param.dx, param.physicalLocation);
        x1 = x0 + param.nx_domain;
        y0 = toLB((T) 0, 1, param.dx, param.physicalLocation);
        y1 = y0 + param.ny_domain;
    }
    virtual bool operator() (plint iX, plint iY) const {
        bool is_absorbing = 0;
        if ( isAbsorbing(param.bcType[0])){    
            if (iX <= x0){
                is_absorbing = 1;
            }                 
        }
        if ( isAbsorbing(param.bcType[1])){    
            if (iX >= x1 - 1){
                is_absorbing = 1;
            }                 
        }
        if ( isAbsorbing(param.bcType[2])){    
            if (iY <= y0){
                is_absorbing = 1;
            }                 
        }
        if ( isAbsorbing(param.bcType[3])){    
            if (iY >= y1 - 1){
                is_absorbing = 1;
            }                 
        }
        
        return is_absorbing;
    }
    virtual AbsorbingBC2D<T>* clone() const {
        return new AbsorbingBC2D<T>(*this);
    }
private:
    T x0;
    T x1;
    T y0;
    T y1;
};


template<typename T>
class AbsorbingNodes{
public:
    AbsorbingNodes()
    { }
    bool operator() (plint iX, plint iY) const {
        return AbsorbingBC2D<T>()(iX, iY);
    }
};

// Locate voxels with reflecting BC and create a Boolean mask
template<typename T>
class ReflectingBC2D : public DomainFunctional2D {
public:
    ReflectingBC2D()
    { 
        x1 = param.nx;
        y1 = param.ny;
    }
    virtual bool operator() (plint iX, plint iY) const {
        bool is_reflecting = 0;
        if ( isFreeslip(param.bcType[0])){    
            if (iX < 1){
                is_reflecting = 1;
            }                 
        }
        if ( isFreeslip(param.bcType[1])){    
            if (iX >= x1 - 1){
                is_reflecting = 1;
            }                 
        }
        if ( isFreeslip(param.bcType[2])){    
            if (iY < 1){
                is_reflecting = 1;
            }                 
        }
        if ( isFreeslip(param.bcType[3])){    
            if (iY >= y1 - 1){
                is_reflecting = 1;
            }                 
        }
        
        return is_reflecting;
    }
    virtual ReflectingBC2D<T>* clone() const {
        return new ReflectingBC2D<T>(*this);
    }
private:
    T x0;
    T x1;
    T y0;
    T y1;
};


template<typename T>
class ReflectingNodes{
public:
    ReflectingNodes()
    { }
    bool operator() (plint iX, plint iY) const {
        return ReflectingBC2D<T>()(iX, iY);
    }
};

// Locate voxels with periodic BC and create a Boolean mask
template<typename T>
class PerioBC2D : public DomainFunctional2D {
public:
    PerioBC2D()
    { 
        x1 = param.nx;
        y1 = param.ny;
    }
    virtual bool operator() (plint iX, plint iY) const {
        bool is_perio = 0;
        if ( isPeriodic(param.bcType[0])){    
            if (iX < 1){
                is_perio = 1;
            }                 
        }
        if ( isPeriodic(param.bcType[1])){    
            if (iX >= x1 - 1){
                is_perio = 1;
            }                 
        }
        if ( isPeriodic(param.bcType[2])){    
            if (iY < 1){
                is_perio = 1;
            }                 
        }
        if ( isPeriodic(param.bcType[3])){    
            if (iY >= y1 - 1){
                is_perio = 1;
            }                 
        }
        
        return is_perio;
    }
    virtual PerioBC2D<T>* clone() const {
        return new PerioBC2D<T>(*this);
    }
private:
    T x0;
    T x1;
    T y0;
    T y1;
};


template<typename T>
class PeriodicNodes{
public:
    PeriodicNodes()
    { }
    bool operator() (plint iX, plint iY) const {
        return PerioBC2D<T>()(iX, iY);
    }
};

// Initial macroscopic fields: velocity is set to 0 and density to the corresponding
// Gaussian pulse ones, which are supersposed.
void getInitialDensityAndVelocity(plint iX, plint iY, T& rho, Array<T,2>& u) {
    u = param.uLB;
    T x = toPhys((T)iX, 0, param.dx, param.physicalLocation);
    T y = toPhys((T)iY, 1, param.dx, param.physicalLocation);
    
    rho = param.rho0;
    
    for (plint i = 0; i < param.totalNumPulses; i++) {
        T alp = log(2.)/(param.b[i]*param.b[i]);
        rho += (param.eps[i]*(exp ((-alp*1.)*((x-param.cx[i])*(x-param.cx[i]) 
                            + (y-param.cy[i])*(y-param.cy[i])) )));
    }
}

// Initialize the lattice at zero velocity and the initial density field.
void defineInitialDensity(
	MultiBlockLattice2D<T,DESCRIPTOR>& lattice)
{
    // Initial pdf are calculated form macroscopic variables at equilibrium.
    initializeAtEquilibrium (
           lattice, lattice.getBoundingBox(), getInitialDensityAndVelocity );

}

// Perfectly Matched Layers (aka sponge zones) functional:
// in this zone, pdf are relaxed towards a reference macroscopic field (rhoF, uF)
// premultiplied by a smooth function Sigma define in
// Brogi et al. 2017 JASA 142(4) 2332-2345, eq. (27) 
template<typename T, template<typename U> class Descriptor>
class InitializeSponges : public BoxProcessingFunctional2D_LS<T,Descriptor,T>
{
    public :
    InitializeSponges(T omega_, Array<plint,4> numCells_)
        : omega(omega_),
          numCells(numCells_)
{ };
virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice,
        ScalarField2D<T>& sigmaField)
{
    Box2D totalDomain(0, param.nx, 0, param.ny); 
    WaveAbsorptionSigmaFunction2D<T> sigmaFunction(totalDomain, numCells, omega); 
    Dot2D relativePosition = lattice.getLocation();
 
    for (plint iX = domain.x0; iX <= domain.x1; ++iX) {
            for (plint iY = domain.y0; iY <= domain.y1; ++iY)
            {
                // Remember that processing functionals coordinates are Box's ones.
                // We must use absolute coordinates for sigma function, defined over the 
                // whole domain.
                plint absoluteX = iX + relativePosition.x; 
                plint absoluteY = iY + relativePosition.y;
                T sigma = sigmaFunction(absoluteX, absoluteY);

                sigmaField.get(iX, iY) = sigma;

                lattice.get(iX,iY).setExternalField (
                        Descriptor<T>::ExternalField::sigmaBeginsAt,
                        Descriptor<T>::ExternalField::sizeOfSigma, &sigma );
                T rhoBarF = param.rho0 - 1;
                lattice.get(iX,iY).setExternalField (
                        Descriptor<T>::ExternalField::rhoBarBeginsAt,
                        Descriptor<T>::ExternalField::sizeOfRhoBar, &rhoBarF );
                // Cannot use plb::Array here as setExternalField only takes (T* ext) (aka a pointer)
                // as argument.
                T uF[2] = {0., 0.};
                lattice.get(iX,iY).setExternalField (
                        Descriptor<T>::ExternalField::uBeginsAt,
                        Descriptor<T>::ExternalField::sizeOfU, &uF[0]  );
            }
        }
};
virtual InitializeSponges<T,Descriptor>* clone() const
{
    return new InitializeSponges<T,Descriptor>(*this);
};
virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const
{
    modified[0] = modif::staticVariables;
    modified[1] = modif::nothing;
};
private :
    T omega;
    Array<plint,4> numCells;
};

void createSpongeZones(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
    MultiScalarField2D<T>& sigma)
{
    T dx = param.dx;

    Array<plint,4> numSpongeCells;
    plint totalNumSpongeCells = 0;
    for (plint iZone = 0; iZone < 4; iZone++) {
        numSpongeCells[iZone] = 0;
        // for each boundary, there can be a sponge zone defined by its number of
        // "sponge voxels". 
        if (isAbsorbing(param.bcType[iZone]) ){        
            numSpongeCells[iZone] = util::roundToInt(param.spongeWidths[iZone] / dx);
        }
        totalNumSpongeCells += numSpongeCells[iZone];
        pcout << "numSpongeCells[" << iZone << "] = " << numSpongeCells[iZone] << std::endl;
    }
    pcout << "totalNumSpongeCells = " << totalNumSpongeCells << std::endl;

    if (totalNumSpongeCells > 0) {
        pcout << "Generating viscosity sponge zone." << std::endl << std::endl;

        applyProcessingFunctional(new InitializeSponges<T,DESCRIPTOR>(param.omega, numSpongeCells),
                                              lattice.getBoundingBox(), lattice, sigma);
    }
    pcout << std::endl;
}

template<typename T>
class VelocityProfile
{
    public :
    VelocityProfile()
    {
        dx = param.dx;
        dt = param.dt;
    };

    void operator()(plint iX, plint iY, Array<T, 2>& u) const
    {

        u[0] = (T) 0.0;
        u[1] = (T) 0.0;
    }

    private :
        T dx;
        T dt;
};

// BCs can be either Velocity Dirichlet (u = 0), freeslip (u.n = 0) or perio
// remember that even with absorbig walls one must define one of the aforementioned
// BCs.
void setVelocityBC(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, 
    OnLatticeBoundaryCondition2D<T,DESCRIPTOR>* bc)
{
    Box2D box = lattice.getBoundingBox();

    std::vector<Box2D> bc_list(4);
    Box2D left(box.x0, box.x0, box.y0, box.y1);
    Box2D right(box.x1, box.x1, box.y0, box.y1);
    Box2D bottom(box.x0+1, box.x1-1, box.y0, box.y0);
    Box2D top(box.x0+1, box.x1-1, box.y1, box.y1);

    bc_list[0] = left; 
    bc_list[1] = right; 
    bc_list[2] = bottom; 
    bc_list[3] = top; 
    
    if ( isPeriodic(param.bcType[0]) && isPeriodic(param.bcType[1]) ){        
        lattice.periodicity().toggle(0, true);
    }
    if ( isPeriodic(param.bcType[2]) && isPeriodic(param.bcType[3]) ){        
        lattice.periodicity().toggle(1, true);
    }

    for (plint i = 0; i < 4; i++) {
        if ( isVelocityDirichlet(param.bcType[i]) ){  
            bc->setVelocityConditionOnBlockBoundaries(lattice, bc_list[i], boundary::dirichlet);
            setBoundaryVelocity(lattice, bc_list[i], VelocityProfile<T>());
        }
        if ( isFreeslip(param.bcType[i]) ){
            bc->setVelocityConditionOnBlockBoundaries(
                    lattice, bc_list[i], boundary::freeslip);
        }
    }
}

// utility for output results in .vti files, for postproc in Paraview or
// with python
void writeVTK(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
          MultiScalarField2D<T>& mask_absorbing,
          MultiScalarField2D<T>& mask_reflecting,
          MultiScalarField2D<T>& mask_periodic,
          plint iter)
{
    T dx = param.dx;
    T dt = param.dt;
    
    T x0 = toLB((T) 0, 0, dx, param.physicalLocation);
    T x1 = x0 + param.nx_domain - 1;
    T y0 = toLB((T) 0, 1, dx, param.physicalLocation);
    T y1 = y0 + param.ny_domain - 1;
    
    Box2D box_phys(x0, x1, y0, y1);
    
    auto rho = extractSubDomain(*computeDensity(lattice), box_phys);
    auto vel = extractSubDomain(*computeVelocity(lattice), box_phys);
    
    auto mask_absorbing_field = extractSubDomain(mask_absorbing, box_phys);
    auto mask_reflecting_field = extractSubDomain(mask_reflecting, box_phys);
    auto mask_periodic_field = extractSubDomain(mask_periodic, box_phys);
    
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<float>(*rho, "density", 1.);
    vtkOut.writeData<2,float>(*vel, "velocity", dx/dt);
    vtkOut.writeData<int>(*mask_absorbing_field, "mask_absorbing", 1.);
    vtkOut.writeData<int>(*mask_reflecting_field, "mask_reflecting", 1.);
    vtkOut.writeData<int>(*mask_periodic_field, "mask_periodic", 1.);
}

std::string format_account_number(int acct_no) {
    char buffer[7];
    std::snprintf(buffer, sizeof(buffer), "%06d", acct_no);
    return buffer;
}


// Main program
int main(int argc, char* argv[]) {
plbInit(&argc, &argv);

string xmlFileName;
try {
    global::argv(1).read(xmlFileName);
}
catch (PlbIOException& exception) {
    pcout << "Wrong parameters; the syntax is: "
          << (std::string)global::argv(0) << " input-file.xml" << std::endl;
    return -1;
}

try {
    param = Param(xmlFileName);
}
catch (PlbIOException& exception) {
    pcout << exception.what() << std::endl;
    return -1;
}
global::IOpolicy().activateParallelIO(true);

// RNG
std::mt19937 gen(param.randomSeed);

std::uniform_int_distribution<plint> uniformPulses(param.statNumRandPulses[0],
            param.statNumRandPulses[1]);
std::uniform_real_distribution<T> uniformCx(param.statCx[0], param.statCx[1]);
std::uniform_real_distribution<T> uniformCy(param.statCy[0], param.statCy[1]);
std::uniform_real_distribution<T> uniformEps(param.statEps[0], param.statEps[1]);
std::uniform_real_distribution<T> uniformB(param.statB[0], param.statB[1]);

// Loop over number of simulations (new initial conditions)
for(plint sim=0; sim < param.numSim;  ++sim) { 
    std::string tmpOutDir = param.outputDir + '/' + format_account_number(sim) + '/';

    if (global::mpi().isMainProcessor()) {
        mkpath(tmpOutDir.c_str(), 0755);
    }

    computeDomainGeometry(param);
    setRandomPulses(param, gen, uniformPulses, uniformCx, uniformCy,
            uniformEps, uniformB);
    computeDerivedParameters(param);

    global::directories().setOutputDir(tmpOutDir + "/");
    
    pcout << "Initialising..." << std::endl;

    MultiBlockLattice2D<T, DESCRIPTOR> lattice (param.nx, param.ny, new RegularizedBGKdynamics<T,DESCRIPTOR>(param.omega) );
    
    MultiScalarField2D<T> sigmaField(param.nx, param.ny);
    MultiScalarField2D<T> absBCMask(param.nx, param.ny);
    MultiScalarField2D<T> reflBCMask(param.nx, param.ny);
    MultiScalarField2D<T> perioBCMask(param.nx, param.ny);

    defineDynamics(lattice, lattice.getBoundingBox(), new RegularizedBGKdynamics<T,DESCRIPTOR>(param.omega));
    
    setCompositeDynamics (
            lattice,
            lattice.getBoundingBox(),
            new WaveAbsorptionDynamics<T,DESCRIPTOR>(new NoDynamics<T,DESCRIPTOR>) );
    
    lattice.toggleInternalStatistics(false);

    pcout << "Generating outer domain boundary conditions." << std::endl;
    OnLatticeBoundaryCondition2D<T,DESCRIPTOR> *bc = createLocalBoundaryCondition2D<T,DESCRIPTOR>();
    setVelocityBC(lattice, bc);
    delete bc; bc = 0;

    defineInitialDensity(lattice);
    createSpongeZones(lattice, sigmaField);
    
    // Create Boolean Masks for BCs position
    {
    MultiScalarField2D<int> flags(param.nx, param.ny);
    setToFunction(flags, flags.getBoundingBox(),
        AbsorbingNodes<T>()); 
    setToConstant(absBCMask, flags, 1, absBCMask.getBoundingBox(), 1.); 
    }
    {
    MultiScalarField2D<int> flags(param.nx, param.ny);
    setToFunction(flags, flags.getBoundingBox(),
        ReflectingNodes<T>()); 
    setToConstant(reflBCMask, flags, 1, reflBCMask.getBoundingBox(), 1.); 
    }
    {
    MultiScalarField2D<int> flags(param.nx, param.ny);
    setToFunction(flags, flags.getBoundingBox(),
        PeriodicNodes<T>()); 
    setToConstant(perioBCMask, flags, 1, perioBCMask.getBoundingBox(), 1.); 
    }

    // Pdfs are initialized here
    lattice.initialize();

    pcout << "Initialisation finished!" << std::endl << std::endl;
    pcout << "Parameters :" << std::endl << std::endl;
	
	printParam(param);    

    // Main loop over time iterations.
    for (plint iT=0; iT<param.maxIter; ++iT) {
	    pcout << "Iteration: " << iT*param.dt << endl;

        if (iT% param.vtkIter == 0) {      
	        pcout << "Output vtk" << endl;
            writeVTK(lattice, absBCMask, reflBCMask, perioBCMask, iT);
        }

        // Execute lattice Boltzmann iteration.
        lattice.collideAndStream();
    }

    // Clean variables for next run
    param.cxLB.clear();
    param.cyLB.clear();

    param.cx.clear();
    param.cy.clear();
    param.eps.clear();
    param.b.clear();
    
    pcout << "End of run " << sim << std::endl << std::endl;

    }

}
