#!/usr/bin/env python
# coding: utf-8

r"""
    Script for autoregressive-test.
    Usage:
        python3 autoregressive_test.py --simConf autoregressive_configuration_example.yaml
    The initial conditions are provided by the LBM generator.
    Then the trained NN is used in an auto-regressive way:
        x_{t+1} = f(x_{t})
    Results are compared with LBM ground truth.
    Figures of output, target, error and slices of fields can be saved.
    Error evolution is saved in .npz format
"""

# native modules
import sys
import glob
import os
import argparse

# third party modules
import yaml
import torch
import numpy as np
import numpy.ma as ma

import matplotlib
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.font_manager as font_manager
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar

# local modules
import pulsenetlib

def insert_colobar(fig, ax, im, **kwargs):
    axins = inset_axes(ax,
       width="100%",
       height="20%",
       loc='lower left',
       bbox_to_anchor=(0.1, 0., 1, 1),
       bbox_transform=ax.transAxes,
       borderpad=0,
       )         
    return fig.colorbar(im, cax=axins, **kwargs)

def auto_fit_fontsize(text, width, height, fig=None, ax=None):
    '''Auto-decrease the fontsize of a text object.

    Args:
        text (matplotlib.text.Text)
        width (float): allowed width in data coordinates
        height (float): allowed height in data coordinates
    '''
    fig = fig or plt.gcf()
    ax = ax or plt.gca()

    # get text bounding box in figure coordinates
    renderer = fig.canvas.get_renderer()
    bbox_text = text.get_window_extent(renderer=renderer)

    # transform bounding box to data coordinates
    bbox_text = matplotlib.transforms.Bbox(ax.transData.inverted().transform(bbox_text))

    # evaluate fit and recursively decrease fontsize until text fits
    fits_width = bbox_text.width < width if width else True
    fits_height = bbox_text.height < height if height else True
    if not all((fits_width, fits_height)):
        text.set_fontsize(text.get_fontsize()-1)
        auto_fit_fontsize(text, width, height, fig, ax)

def set_fonts():
    fpath = os.path.join(plt.rcParams["datapath"], "fonts/pdfcorefonts")
    font_files = font_manager.findSystemFonts(fontpaths=fpath, fontext='afm')
    font_list = font_manager.createFontList(font_files)
    font_manager.fontManager.ttflist.extend(font_list)

    latex_style_times = matplotlib.RcParams(
            {
                'font.family': 'stixgeneral',
                'mathtext.fontset': 'stix',
                'text.usetex': False,
            })
    plt.rcParams['axes.unicode_minus'] = False
    plt.style.use(latex_style_times)

def cli_main():
    parser = argparse.ArgumentParser(description='Test Propagation. \n'
            'Read yaml config file for more information')
    parser.add_argument('--simConf',
            help='R|Simulation yaml configuration file.')

    arguments = parser.parse_args()

    # read configuration file
    simConfPath = arguments.simConf
    with open(simConfPath, 'r') as f:
        simConf = yaml.load(f, Loader=yaml.FullLoader)

    # prepare device
    device = torch.device('cpu')
    if torch.cuda.is_available():
        print('Active CUDA Device: GPU', torch.cuda.current_device())
        device = torch.device('cuda')

    # read configuration files and loading model from the folder
    model_path = simConf['modelPath']
    net, state_dict, train_configuration, model_configuration = \
        pulsenetlib.datatransfer.load_model(
            model_path, run_device=device)

    # initate model
    with torch.no_grad():
        net.to(device)
        net.eval()

        net.load_state_dict(state_dict)
        
        #*********************** Simulation parameters ***********************
        channels = model_configuration['channels']
        number_channels = len(channels)
        input_frames = model_configuration['numInputFrames']
        output_frames = model_configuration['numOutputFrames']
        total_number_frames = input_frames + output_frames
        scalar_add = torch.tensor(
            model_configuration['scalarAdd']).to(device=device)
        scalar_mul = torch.tensor(model_configuration['scalarMul'], device=device)
        frame_step = train_configuration['frameStep']

        channel = 0

        max_iter = simConf['maxIter'] # time horizon
        stat_freq = simConf['statFreq'] # frequency of ouptut
        start_io = simConf['startIO'] # initial output iteration
        if start_io >= max_iter:
            raise ValueError('startIO must be smaller than maxIter')
        apply_EPC = simConf['correction']
        
        init_t = 0
        if 't0' in simConf:
            init_t = simConf['t0']
        print('Initial iteration: ' + str(init_t))

        #**************** Data *********************************************
        datadir = os.path.join(simConf['dataPath'], simConf['dataset'])
        format_files = 'vtk{0:06d}.vti'

        # load one file to infer field size
        vtkloader = pulsenetlib.preprocess.VTKLoader(channels)
        input_file = os.path.join(datadir, format_files.format(0))
        _, nx, ny, _ = vtkloader.load(input_file)

        dtype = torch.float

        data = torch.zeros((1, number_channels, input_frames, nx, ny),
                dtype=dtype, device=device)
        initial = torch.zeros((1, number_channels, 1, nx, ny),
                dtype=dtype, device=device)
        reference = torch.zeros((1, number_channels, output_frames, nx, ny),
                dtype=dtype, device=device)

        
        mae = torch.nn.L1Loss()
        mse = torch.nn.MSELoss()
        error_np = np.empty((max_iter-input_frames+1, 9))

        for it in range(0, input_frames):
            idx = int((init_t + it)* frame_step)
            input_file = os.path.join(datadir, format_files.format(idx))
            data[:,:,it,:,:], _, _, _ = vtkloader.load(input_file)

        # performing data preparation(for example, using
        # acoustic pressure instead of pressure)
        pulsenetlib.transforms.offset_(data, scalar_add)
        pulsenetlib.transforms.multiply_(data, scalar_mul)

        initial = data[:,0,0].unsqueeze(1).unsqueeze(1)

        X, Y = np.linspace(0, nx-1, num=nx), np.linspace(0, ny-1, num=ny)

        #********************* Figure ***************************************
        figsize = (3,3)
        
        output_folder = simConf['outputPath']
        if (not os.path.exists(output_folder)):
            os.makedirs(output_folder, exist_ok=True)
        
        plotType = simConf['plots']
        plotTar = plotType['Target'] == 1
        plotNN = plotType['NN'] == 1
        plotError = plotType['Error'] == 1
        plotSliceAbs = plotType['Slices'] == 1
        
        print('Plot characteristics:')
        print('  Target = ' + str(plotTar))
        print('  NN     = ' + str(plotNN))
        print('  Error  = ' + str(plotError))
        print('  Slices = ' + str(plotSliceAbs))

        # slices
        slice_line = simConf['slice']
        n_slices = len(slice_line)
        
        x0sl = []
        y0sl = []
        x1sl = []
        y1sl = []
        slice_amplitude = []
        for s in slice_line:
            if np.any(np.less(np.array(s[:4]), 0.)) or np.any(np.greater(np.array(s[:4]), 1.)):
                raise ValueError(f'Distances in simConf["slice"] are relative to domain length.'
                                 f'Expected range is [0,1]. Found values are {s[:4]}')
            x0sl.append(int(s[0]*(nx-1)))
            y0sl.append(int(s[1]*(ny-1)))
            x1sl.append(int(s[2]*(nx-1)))
            y1sl.append(int(s[3]*(ny-1)))
            slice_amplitude.append(s[4])

        max_slice = np.zeros(n_slices)
        min_slice = np.zeros(n_slices)

        # matplotlib params
        # linewidth (slices)
        lw = 1.5

        # Markers (slices)
        marker='s'
        markevery_nn = 10
        markersize_nn = 5
        
        # Fontsize
        SIZE = 20
        plt.rc('font'  , size=SIZE)       # controls default text sizes
        plt.rc('axes'  , titlesize=SIZE)  # fontsize of the axes title
        plt.rc('axes'  , labelsize=SIZE)  # fontsize of the x and y labels
        plt.rc('xtick' , labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('ytick' , labelsize=SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SIZE)   # legend fontsize
        plt.rc('figure', titlesize=SIZE)  # fontsize of the figure title
        
        # Max and min levels
        vmin = -2e-4
        vmax = 2e-4
        num_levels = 20
        vstep = (vmax - vmin)/num_levels
        power_ten = -4

        # colormaps
        field_map = cm.get_cmap("seismic",lut=num_levels+1)
        newcolors = [plt.cm.tab10(i) for i in range(4)]
        mask_map = ListedColormap(newcolors, name='mask')

        mask_map.set_bad('white')
        field_map.set_bad('gray')
        err_map = cm.Reds
        err_map.set_bad('gray')

        max_err_level = simConf['error']['max']
        err_num_levels = int(simConf['error']['numLevels'])
        err_levels = np.linspace(0, max_err_level, num=err_num_levels)
        
        last = (max_iter)//stat_freq * stat_freq
        ticks_bar = [vmin, vmin/2, 0.0, vmax/2, vmax]
        ticks_bar_err = err_levels[::4] 
        
        #**************************Output Context mask ************************
        if number_channels > 1: 
            mask_np = torch.squeeze(data[:,1:]).cpu().data.numpy()
            context_np = np.zeros((nx, ny))
            for i, chan in enumerate(channels):
                if chan == 'mask_absorbing':
                    context_np[mask_np[i]==1] = 1
                if chan == 'mask_reflecting':
                    context_np[mask_np[i]==1] = 2
                if chan == 'mask_periodic':
                    context_np[mask_np[i]==1] = 3

            left = context_np[0,ny//2] 
            right = context_np[-1,ny//2] 
            bottom = context_np[nx//2,0] 
            top = context_np[nx//2,-1] 

            context_np = np.zeros((10,10))
            context_np[0,1:-1] = left
            context_np[-1,1:-1] = right
            context_np[1:-1,0] = bottom
            context_np[1:-1,-1] = top
            context_np[0,-1] = 4
            context_np[-1,-1] = 4
            context_np[0,0] = 4
            context_np[-1,0] = 4
            context_np[context_np==0] = np.nan
        
            fig = Figure(figsize=figsize)
            canvas=FigureCanvas(fig)
            ax_p_tar = fig.add_subplot(111)
            ax_p_tar.imshow(context_np,
                cmap=mask_map,
                extent=[0,10,0,10],
                interpolation=None,
                origin='lower')
            ax_p_tar.axis('on')
            ax_p_tar.set_xlabel('')
            ax_p_tar.set_ylabel('')
            x0,x1 = ax_p_tar.get_xlim()
            y0,y1 = ax_p_tar.get_ylim()
            ax_p_tar.xaxis.set_major_locator(ticker.MultipleLocator(1))
            ax_p_tar.yaxis.set_major_locator(ticker.MultipleLocator(1))
            ax_p_tar.xaxis.set_minor_locator(ticker.MultipleLocator(1))
            ax_p_tar.yaxis.set_minor_locator(ticker.MultipleLocator(1))
            ax_p_tar.grid(True, which='both', color='grey', linestyle='-', linewidth=1)
            ax_p_tar.set_xticks([],minor=[])
            ax_p_tar.set_yticks([],minor=[])
            ax_p_tar.set_aspect((x1-x0)/(y1-y0))
            filename = os.path.join(output_folder, f'input_context.png')
            fig.savefig(filename, bbox_inches='tight')

        #***********************Outputing padding mode ************************
        fig = Figure(figsize=figsize)
        canvas=FigureCanvas(fig)
        ax_p_tar = fig.add_axes([0,0,0.7,0.3])
        ax_p_tar.text(0.5, 0.5,
            model_configuration['paddingMode'].upper(),
            verticalalignment='center',
            horizontalalignment='center',
            fontsize=24)
        ax_p_tar.axis('off')
        ax_p_tar.set_xlabel('')
        ax_p_tar.set_ylabel('')
        ax_p_tar.set_xticks([],minor=[])
        ax_p_tar.set_yticks([],minor=[])

        x0,x1 = ax_p_tar.get_xlim()
        y0,y1 = ax_p_tar.get_ylim()
        filename = os.path.join(output_folder, f'padding.png')
        fig.savefig(filename, bbox_inches='tight')

        #************************Initial conditions output ********************
        data_np = torch.squeeze(data[:,0]).cpu().data.numpy() / scalar_mul[0].item()
        for i in range(0, input_frames):
            if plotTar:
                fig = Figure(figsize=figsize)
                canvas=FigureCanvas(fig)
                ax_p_tar = fig.add_subplot(111)
                if channel == 0:
                    ax_p_tar.imshow(data_np[i],
                            vmin=vmin-vstep/2, vmax=vmax+vstep/2,
                            cmap=field_map,
                            origin='lower')
                    ax_p_tar.axis('on')
                    ax_p_tar.set_xlabel('')
                    ax_p_tar.set_ylabel('')
                    ax_p_tar.set_xticks([],minor=[])
                    ax_p_tar.set_yticks([],minor=[])

                x0,x1 = ax_p_tar.get_xlim()
                y0,y1 = ax_p_tar.get_ylim()
                ax_p_tar.set_aspect((x1-x0)/(y1-y0))
                filename = os.path.join(output_folder, f'input_{i}.pdf')
                fig.savefig(filename, bbox_inches='tight', format='pdf')

        it = input_frames
        neverComeBack = False
        firstOut = True

        print('\nStarting Iterations\n')

        #************************ Main auto-regressive loop*******************
        while (it < max_iter+1):
            idx = (init_t + it) * frame_step
            reference_file = os.path.join(datadir, format_files.format(idx))
            reference[0,:,0,:,:], _, _, _ = vtkloader.load(reference_file)

            # data transforms
            pulsenetlib.transforms.offset_(reference, scalar_add)
            pulsenetlib.transforms.multiply_(reference, scalar_mul)
            
            # remove mask from target for loss calculation
            data_channels = torch.arange(
                net.num_output_channels, dtype=torch.long, device=device
                ) 
            target = reference.index_select(dim=1, index=data_channels)/ scalar_mul[0].item()

            # perform prediction
            output = net(data.clone())
            if apply_EPC:
                pulsenetlib.acoustics.energyPreservingCorrection(data[:,0].unsqueeze(0), output)

            # "Roll" first channel to the last; second to the first etc
            data = torch.cat((data[:,:,1:,:,:], data[:,:,:1,:,:]), dim=2)
            # Replace last frame with the new output
            data[:,0,-1,:,:] = output[:,0,:,:,:].clone()

            output.div_(scalar_mul[0].item())

            #***********************Post-processing****************************
            gradx_out = pulsenetlib.tools.derivate_dx(output,  dx=1.)
            grady_out = pulsenetlib.tools.derivate_dy(output,  dx=1.)
            gradx_tar = pulsenetlib.tools.derivate_dx(target,  dx=1.)
            grady_tar = pulsenetlib.tools.derivate_dy(target,  dx=1.)
            alpha = 2
            gdl = torch.abs(  gradx_out -  gradx_tar )**alpha + \
                  torch.abs(  grady_out -  grady_tar )**alpha

            output_np = torch.squeeze(output[:,0]).cpu().data.numpy()
            target_np = torch.squeeze(target[:,0]).cpu().data.numpy()

            p_rms = torch.sqrt(torch.mean(target**2))
            gradp_rms = torch.sqrt(torch.mean(gradx_tar**2))

            error_np[it - input_frames, 0] = it + init_t
            error_np[it - input_frames, 1] = mae(output, target).data.item()
            error_np[it - input_frames, 2] = mse(output, target).data.item()
            error_np[it - input_frames, 3] = torch.max(torch.abs(output- target)).data.item()
            error_np[it - input_frames, 4] = p_rms.data.item()
            error_np[it - input_frames, 5] = torch.max(torch.abs(target)).data.item()
            error_np[it - input_frames, 6] = torch.sqrt(torch.mean(gdl)).data.item()
            error_np[it - input_frames, 7] = gradp_rms.data.item()
            error_np[it - input_frames, 8] = torch.max( torch.max(
                                                    torch.abs(gradx_tar), torch.abs(grady_tar) )).data.item()
            rms_err_np = np.sqrt((output_np - target_np)**2)/p_rms.item()
            
            if (it% stat_freq == 0 and it >= start_io):
                print('Iteration {}'.format(it))
                idx = it * frame_step
                # target field
                if plotTar :
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_p_tar = fig.add_subplot(111)
                    im_tar = ax_p_tar.imshow(target_np,
                        vmin=vmin-vstep/2, vmax=vmax+vstep/2,
                        cmap=field_map,
                        origin='lower')
                    for j in range(n_slices):
                        ax_p_tar.plot([x0sl[j]/(nx-1), x1sl[j]/(nx-1)], [y0sl[j]/(ny-1), y1sl[j]/(ny-1)],
                                linewidth=lw, c='k')

                    ax_p_tar.axis('on')
                    ax_p_tar.set_xlabel('')
                    ax_p_tar.set_ylabel('')
                    ax_p_tar.set_xticks([],minor=[])
                    ax_p_tar.set_yticks([],minor=[])
                    x0,x1 = ax_p_tar.get_xlim()
                    y0,y1 = ax_p_tar.get_ylim()
                    ax_p_tar.set_aspect((x1-x0)/(y1-y0))

                    filename = os.path.join(output_folder, f'target_{it}.pdf')
                    fig.savefig(filename, bbox_inches='tight', format='pdf')
                
                # prediction field
                if plotNN:
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_p = fig.add_subplot(111)
                    im_p = ax_p.imshow(output_np,
                        vmin=vmin-vstep/2, vmax=vmax+vstep/2,
                        cmap=field_map,
                        origin='lower')
                    ax_p.axis('on')
                    ax_p.set_xlabel('')
                    ax_p.set_ylabel('')
                    ax_p.set_xticks([],minor=[])
                    ax_p.set_yticks([],minor=[])
                    x0,x1 = ax_p.get_xlim()
                    y0,y1 = ax_p.get_ylim()
                    ax_p.set_aspect((x1-x0)/(y1-y0))

                    filename = os.path.join(output_folder, f'output_{it}.pdf')
                    fig.savefig(filename, bbox_inches='tight', format='pdf')

                    if it == last:
                        fig = Figure(figsize=(6,3))
                        canvas=FigureCanvas(fig)
                        ax = fig.add_subplot(111)
                        insert_colobar(fig, ax, im_p,
                                       ticks=ticks_bar,
                                       format='%.0e',
                                       orientation='horizontal')
                        ax.remove()
                        filename = os.path.join(output_folder, f'cbar.pdf')
                        fig.savefig(filename, bbox_inches='tight', format='pdf')

                # error field
                if plotError:
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_err = fig.add_subplot(111)
                    im_err = ax_err.contourf(X/(nx-1), Y/(ny-1), rms_err_np,
                        levels = err_levels,
                        extend='max',
                        cmap= err_map,
                        origin='lower')
                    ax_err.axis('on')
                    ax_err.set_xlabel('')
                    ax_err.set_ylabel('')
                    ax_err.set_xticks([],minor=[])
                    ax_err.set_yticks([],minor=[])
                    x0,x1 = ax_err.get_xlim()
                    y0,y1 = ax_err.get_ylim()
                    ax_err.set_aspect((x1-x0)/(y1-y0))
                    
                    filename = os.path.join(output_folder, f'error_{it}.pdf')
                    fig.savefig(filename, bbox_inches='tight', format='pdf')
                    
                    if it == last:
                        fig = Figure(figsize=(6,3))
                        canvas=FigureCanvas(fig)
                        ax = fig.add_subplot(111)
                        insert_colobar(fig, ax, im_err,
                                       ticks=ticks_bar_err,
                                       format='%.0e',
                                       orientation='horizontal')
                        ax.remove()
                        filename = os.path.join(output_folder, f'cbar_error.pdf')
                        fig.savefig(filename, bbox_inches='tight', format='pdf')
                        insert_colobar(fig, ax_err, im_err)

                # slices of fields
                for j in range(n_slices):
                    P_out = pulsenetlib.visualize.slice2DField(X, Y, output_np, x0=x0sl[j], y0=y0sl[j], x1=x1sl[j], y1=y1sl[j])
                    P_tar = pulsenetlib.visualize.slice2DField(X, Y, target_np, x0=x0sl[j], y0=y0sl[j], x1=x1sl[j], y1=y1sl[j])

                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_slice = fig.add_subplot(111)
                    if firstOut:
                        max_slice[j] = slice_amplitude[j]
                        min_slice[j] = -slice_amplitude[j]

                        ax_slice.plot(np.arange(P_out.shape[0])/(P_out.shape[0]-1), P_out/(10**power_ten),
                                linewidth=lw,
                                c= 'r',
                                marker=marker, markevery=markevery_nn, \
                                markersize=markersize_nn, markerfacecolor='None')
                        ax_slice.plot(np.arange(P_tar.shape[0])/(P_tar.shape[0]-1), P_tar/(10**power_ten), linewidth=lw, c='k')
                        ax_slice.set_xlim([0, 1])
                        ax_slice.set_ylim([min_slice[j]/(10**power_ten), max_slice[j]/(10**power_ten)])
                        ax_slice.set_ylabel(r'$\rho\prime$ '+r'[$\times$10^{:1.0f}]'.format(power_ten),  fontsize=12)
                        x0,x1 = ax_slice.get_xlim()
                        y0,y1 = ax_slice.get_ylim()
                        ax_slice.set_xticks([0.25, 0.5, 0.75])
                        ax_slice.set_aspect((x1-x0)/(y1-y0))
                        ax_slice.tick_params(which='major',
                                width=0.3,
                                length=0.1,
                                grid_linewidth=0.2,
                                grid_linestyle='--',
                                grid_color='k', labelsize=12)
                        ax_slice.tick_params(which='minor',
                                width=0.2,
                                length=0.05,
                                grid_linewidth=0.15,
                                grid_linestyle='--',
                                grid_color='k')
                        ax_slice.yaxis.set_major_locator(ticker.MultipleLocator((y1-y0)/4))
                        ax_slice.yaxis.set_minor_locator(ticker.MultipleLocator((y1-y0)/8))
                        ax_slice.grid(which='both')
                    else:
                        if it == last:
                            ax_slice.plot(np.arange(P_out.shape[0])/(P_out.shape[0]-1), P_out/(10**power_ten), linewidth=lw,
                                    label='NN',  c= 'r',  marker=marker, markevery=markevery_nn,
                                    markersize=markersize_nn, markerfacecolor='None')
                            ax_slice.plot(np.arange(P_tar.shape[0])/(P_tar.shape[0]-1), P_tar/(10**power_ten),
                                    linewidth=lw, c='k',
                                    label='Target')
                        else:
                            ax_slice.plot(np.arange(P_out.shape[0])/(P_out.shape[0]-1), P_out/(10**power_ten), linewidth=lw,
                                    c= 'r',  marker=marker, markevery=markevery_nn,
                                    markersize=markersize_nn, markerfacecolor='None')
                            ax_slice.plot(np.arange(P_tar.shape[0])/(P_tar.shape[0]-1), P_tar/(10**power_ten),
                                    linewidth=lw, c='k')
                        ax_slice.set_xlim([0, 1])
                        ax_slice.set_ylim([min_slice[j]/(10**power_ten), max_slice[j]/(10**power_ten)])
                        x0,x1 = ax_slice.get_xlim()
                        y0,y1 = ax_slice.get_ylim()
                        ax_slice.set_aspect((x1-x0)/(y1-y0))
                        ax_slice.tick_params(which='major',
                                width=0.3,
                                length=0.1,
                                grid_linewidth=0.2,
                                grid_linestyle='--',
                                grid_color='k', labelsize=12)
                        ax_slice.tick_params(which='minor',
                                width=0.2,
                                length=0.05,
                                grid_linewidth=0.15,
                                grid_linestyle='--',
                                grid_color='k')
                        ax_slice.set_xticks([0.25, 0.5, 0.75])
                        ax_slice.yaxis.set_major_locator(ticker.MultipleLocator((y1-y0)/4))
                        ax_slice.yaxis.set_minor_locator(ticker.MultipleLocator((y1-y0)/8))

                        ax_slice.grid(which='both')
                        lines, labels = ax_slice.get_legend_handles_labels()
                        if it == last:
                            legend = ax_slice.legend(facecolor='w', edgecolor='w', bbox_to_anchor=(1, 0.5), loc="center left")
                    filename = os.path.join(output_folder, f'slice_{it}.png')
                    fig.savefig(filename, bbox_inches='tight')


                if not neverComeBack:
                    firstOut = False
                    neverComeBack = True
            # Update iterations
            it += 1

        filename_error_save = os.path.join(output_folder, 'error_time_evolution')
        print(f'\nSaving Error evolution (numpy format)\n'
              f' to {filename_error_save}.npz')
        np.savez(filename_error_save,
                 it=error_np[:,0],
                 mae=error_np[:,1],
                 mse=error_np[:,2],
                 max=error_np[:,3],
                 rms=error_np[:,4],
                 maxtar=error_np[:,5],
                 gdl=error_np[:,6],
                 gradprms=error_np[:,7],
                 maxgradtar=error_np[:,8])

        print('End of simulation \n')

if __name__ == "__main__":
    cli_main()







