# Acoustic source propagation using deep learning methods

## Install dependencies

We recommend using a `conda environment` to install all the project dependencies, listed in the [requirements.txt](requirements.txt) file.

**Disclaimer**: This project has been tested on Ubuntu 18.04 and in a Red Hat Enterprise Linux Server release 7.5 (Maipo).

```
conda create --name <myEnvName>
conda activate <myEnvName>
conda install pip
pip install -r requirements.txt
```

## Generating the data

You can generate the databases of acoustic propagating waves with the Palabos library project in the [`data_generation`](../data_generation/) folder.

## Preprocessing the data

First you need to preprocess the previously generated `.vti` files into `.pt` (native pytorch format), so that the loading of data is done more efficiently. The `configuration.yaml` has the following parameters that affect the data preprocessing step:

**Data parameters :**
This category of params handles the location of the data, as well as the format of files.

<table border="1" align="center">
  <tbody>
    <tr>
      <td align="center"><b>parameter</b></td>
      <td align="center"><b>example</b></td>
      <td align="center"><b>description</b></td>
    </tr>
    <tr>
      <td><code>dataPathRaw</code></td>
      <td align="center"><code>'path/to/raw/datbase'</code></td>
      <td>path to the folder with the <b>raw</b> dataset (simulation data) for training</td>
    </tr>
    <tr>
      <td><code>dataPath</code></td>
      <td align="center"><code>'{:06d}/vtk{:06d}.vt'</code></td>
      <td>path to the folder with <b>preprocessed</b> database for training (automatically created after first preprocessing)./td>
    </tr>
    <tr>
      <td><code>formatVTK</code></td>
      <td align="center"><code>'{:06d}/vtk{:06d}.vti'</code></td>
      <td>format of the raw (simulation) data filename</code></td>
    </tr>
    <tr>
      <td><code>formatPT</code></td>
      <td align="center"><code>'scene{:06d}_group{:05d}.pt'</code></td>
      <td>format of the preprocessed data filename</td>
    </tr>
    <tr>
      <td><code>frameStep</code></td>
      <td align="center"><code>1</code></td>
      <td>interval between solution steps (must be a multiple of the LBM recorded values)</td>
    </tr>
    <tr>
      <td><code>numWorkers</code></td>
      <td align="center"><code>0</code></td>
      <td>number of parallel workers for dataloader (if 0, selected by pytorch)</td>
    </tr>    
    <tr>
      <td><code>numPreprocThreads</code></td>
      <td align="center"><code>12</code></td>
      <td>number of parallel workers for preprocessing step</td>
    </tr>    
  </tbody>
</table>

**Model parameters :**
Parameters that affect the model definition (e.g. input channels, type of data)

<table border="1" align="center">
  <thead>  
  </thead>
  <tbody>
    <tr>
      <td align="center"><b>parameter</b></td>
      <td align="center"><b>example</b></td>
      <td align="center"><b>description</b></td>
    </tr>
    <tr>
      <td><code>model</code></td>
      <td align="center"><code>'implicitBC'</code></td>
      <td>A MultiScale CNN is employed. Chose here how to treat boundary conditions. Options: <code>'implicitBC'</code> (padding only or padding+context), <code>'explicitBC'</code>(enforcing BC at output)</td>
    </tr>
    <tr>
      <td><code>numInputFrames</code></td>
      <td align="center"><code>4</code></td>
      <td>how many data frames will be used as input</code></td>
    </tr>
    <tr>
      <td><code>numOutputFrames</code></td>
      <td align="center"><code>1</code></td>
      <td>how many data frames will be output (and compared with a target frame during training)</td>
    </tr>
    </tr>
    <tr>
      <td><code>channels</code></td>
      <td align="center"><code>['pressure', 'mask_absorbing']</code></td>
      <td>name of the quantities to consider from preprocessing to training. Options: <code>pressure</code>, <code>mask_absorbing</code>, <code>mask_periodic</code>, <code>mask_absorbing</code></td>
    </tr>    
    <tr>
      <td><code>outputChannels</code></td>
      <td align="center"><code>['pressure']</code></td>
      <td>name of the quantities to predict at output. Options: Among those available in <code>channels</code></td>
    </tr>    
    <tr>
      <td><code>spatialContext</code></td>
      <td align="center"><code>['mask_absorbing']</code></td>
      <td>Name of the quantities to employed as spatial context. This context is concatenated to the input. Options: Among those available in <code>channels</code> or leave empty if none is used</td>
    </tr>    
    <tr>
      <td><code>paddingMode</code></td>
      <td align="center"><code>'replicate'</code></td>
      <td>Type of padding employed in the CNN. Options: <code>'zeros'</code>, <code>'circular'</code>, <code>'replicate'</code>, <code>'reflect'</code> </td>
    </tr>    
  </tbody>
</table>

Once configured, you can launch the data preprocessing by executing:

```
python train.py --trainingConf configuration.yaml --preproc
```

## Training the model

To train the model, edit again the `configuration.yaml` file with the parameters listed below. Then run this command:

```
python train.py --trainingConf configuration_train.yaml
```

Additional flags:

- `--verbose`: display evolution of training and epoch statistics on terminal

**Paramaters**  

<table border="1" align="center">
  <thead>  
  </thead>
  <tbody>
    <tr>
      <td align="center"><b>parameter</b></td>
      <td align="center"><b>example</b></td>
      <td align="center"><b>description</b></td>
    </tr>
    <tr>
      <th colspan="3" align="center">Data and output paramaters</th>
    </tr>
    <tr>
      <td><code>numWorkers</code></td>
      <td align="center"><code>0</code></td>
      <td>number of parallel workers for dataloader (if 0, selected by pytorch)</td>
    </tr>
    <tr>
      <td><code>modelDir</code></td>
      <td align="center"><code>'/path/to/model/dir'</code></td>
      <td>output folder for trained model and loss log</td>
    </tr>
    <tr>
      <td><code>version</code></td>
      <td align="center"><code>'test'</code></td>
      <td>experiment version</td>
    </tr>
    <tr>
      <td><code>modelFilename</code></td>
      <td align="center"><code>'convModel'</code></td>
      <td>trained model name</td>
    </tr>
    <tr>
      <th colspan="3" align="center">Training parameters</th>
    </tr>
    <tr>
      <td><code>resume</code></td>
      <td align="center"><code>false</code></td>
      <td>Resume training from checkpoint</td>
    </tr>
    <tr>
      <td><code>maxEpochs</code></td>
      <td align="center"><code>100</code></td>
      <td>Maximum number of training epochs</td>
    </tr>
    <tr>
      <td><code>batchSize</code></td>
      <td align="center"><code>32</code></td>
      <td>Batch size</td>
    </tr>
    <tr>
      <td><code>maxDatapoints</code></td>
      <td align="center"><code>0</code></td>
      <td>Maximum number of datapoints (if 0, all available datapoints are used). A datapoint is the group of frames that is composed by the merge of the input and target frames. When the maximun number of datapoints is smaller than the number of available groups, a reduced number of groups per scene (simulation) is selected rather than a few complete scenes, aiming at a more diverse database</td>
    </tr>
    <tr>
      <td><code>selectMode</code></td>
      <td align="center"><code>first</code></td>
      <td>How the group of frames are selected from the scenes during preprocessing. For <strong>n</strong> groups:<br/>
      <ul>
        <li><code>first</code>: first <strong>n</strong> groups (start of the scene)</li>
        <li><code>last</code>: last <strong>n</strong> groups (end of the scene)</li>
        <li><code>random</code>: random <strong>n</strong> groups, uniform distribution</li>
      </ul>
      <code>first</code> and <code>last</code> options will only change behaviour of the preprocessing when the selected number of datapoints is smaller then the number of available groups; <code>random</code> will only change the order of the groups.
      </td>
    </tr>
    <tr>
      <td><code>shuffleTraining</code></td><td align="center"><code>true</code></td><td>Shuffle data batches during training</td>
    </tr>
    <tr>
      <td><code>flipData</code></td><td align="center"><code>true</code></td><td>Flips data fields during training</td>
    </tr>
    <tr>
      <td colspan="3" align="center"><b>Optimizer and scheduler parameters</b></td>
    </tr>
    <tr>
      <td colspan="3" align="center"><ins>Optimizer parameters</ins> - Adam</td>
    </tr>
    <tr>
      <td><code>lr</code></td>
      <td align="center"><code>1.0e-4</code></td>
      <td>learning rate</td>
    </tr>
    <tr>
      <td><code>weight_decay</code></td>
      <td align="center"><code>0.0</code></td>
      <td>weight decay</td>
    </tr>    
    <tr>
      <td colspan="3" align="center"><ins>Scheduler parameters</ins></td>
    </tr>
    <tr>
      <td><code>type</code></td>
      <td align="center"><code>'reduceLROnPlateau'</code></td>
      <td>scheduler type</td>
    </tr>
    <tr>
      <td colspan="3" align="center"><b>Model parameters</b></td>
    </tr>
    <tr>
      <td colspan="3" align="center"><ins>Data preparation</ins></td>
    <tr>
      <td><code>normalization</code></td>
      <td align="center"><code>['gaussian', 'none']</code></td>
      <td>Normalization operations to be performed, as sorted by <code>channels</code>. Options: <code>'gaussian'</code> (Gaussian norm by avg and std, cannot be used on spatialContext channels), <code>'none'</code> (spatialContext channels should be using this)</td>
    </tr>
    <tr>
      <td><code>scalarAdd</code></td>
      <td align="center"><code>[-1.0, 0.0]</code></td>
      <td>operations to be performed per channel, as sorted by <code>channels</code>. Scalar to add to the complete field</td>
    </tr>
    <tr>
      <td><code>scalarMul</code></td>
      <td align="center"><code>[1.0, 1.0]</code></td>
      <td>operations to be performed per channel, as sorted by <code>channels</code>. Scalar to multiply to the complete field</td>
    </tr>
    <tr>
      <td><code>stdNorm</code></td>
      <td align="center"><code>[1.0]</code></td>
      <td>weighting for the division by the std of the first frame, ONLY for channels with <code>'normalization'</code> set to <code>'gaussian'</code></td>
    </tr>
    <tr>
      <td><code>avgRemove</code></td>
      <td align="center"><code>[0.0]</code></td>
      <td>weighting of the average component removal</td>
    </tr>
    <tr>
      <td colspan="3" align="center"><ins>Loss terms</ins> - <code>fooLambda</code>: weigthing for each loss term (set to 0 to disable), for each output channel.</td>
    </tr>
    <tr>
      <td><code>L2Lambda</code></td>
      <td align="center"><code>[1.0]</code></td>
      <td>Mean-squared error of output channels</td>
    </tr>
    <tr>
      <td><code>GDLLambda</code></td>
      <td align="center"><code>[1.0]</code></td>
      <td>Mean-squared error of gradients of output channels</td></tr>
    <tr>
      <td colspan="3" align="center"><b>Training monitoring</b></td>
    </tr>
    <tr>
      <td><code>freqToFile</code></td>
      <td align="center"><code>1</code></td>
      <td>epoch frequency for loss output to disk</td>
    </tr>
  </tbody>
</table>

Check the PyTorch [documentation](https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate) for more information on schedulers.

## Logger

Two loggers are available:
- `np`: saves losses in numpy format to disk (Default)
- `tb`: tensorboard logger

The user can specify it with arguments:

```
python main.py --trainingConf configuration_train.yaml --logger tb
```

## Testing the model

Script `autoregressive_test.py` contains the testing procedure implementation. It is a auto-regressive analysis, that is, previous estimations are used as input to advance in time. 
A configuration file (example in `autoregressive_configuration_example.yaml`) must be provided to perform the testing. Test parameters are described next:

<table border="1" align="center">
  <tbody>
    <tr >
      <td align="center"><b>parameter</b></td>
      <td align="center"><b>example</b></td>
      <td align="center"><b>description</b></td>
    </tr>
    <tr>
      <td><code>dataPath</code></td>
      <td align="center"><code>'/path/to/data'</code></td>
      <td>path to dataset folder. Must contain a <<code>dataset</code>> folder within, with vti files for initial conditions and ground truth.</td>
    </tr>
    <tr>
      <td><code>dataset</code></td>
      <td align="center"><code>'000000'</code></td>
      <td>folder inside <code>'dataPath'</code> where vti files are actually located </td>
    </tr>
    <tr>
      <td><code>modelPath</code></td>
      <td align="center"><code>'/path/to/model'</code></td>
      <td>path to trained model folder. Must contain a <code>'checkpoints'</code> folder within, with the .ckpt files</td>
    </tr>
    <tr>
      <td><code>maxIter</code></td>
      <td align="center"><code>10</code></td>
      <td>number of auto-regressive iterations that will be performed</td>
    </tr>
    <tr>
      <td><code>correction</code></td>
      <td align="center"><code>true</code></td>
      <td>apply energy-preserving correction</td>
    </tr>
    <tr>
      <td><code>outputPath</code></td>
      <td align="center"><code>/path/to/results'</code></td>
      <td>output folder</td>
    </tr>
    <tr>
      <td><code>statFreq</code></td>
      <td align="center"><code>4</code></td>
      <td>frequency of result output (in auto-regressive iterations)</td>
    </tr>
  </tbody>
</table>
    
In order to run a test, just execute:
```
python3 autoregressive_test.py --simConfig autoregressive_configuration_example.yaml 
```

Configuration files employed in the paper are shown in `test_config_files`, for one initial
condition (centered Pulse case). It is straightforward to employ them for other initial conditions, just change `dataPath` accordingly.