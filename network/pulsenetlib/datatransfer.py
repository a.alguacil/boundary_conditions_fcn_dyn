#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions for importing and exporting the model, losses evolution and
configuration files.

"""

# native modules
import os
import glob
import shutil
import importlib.util

# third-party modules
import torch
import numpy as np
import yaml

from pytorch_lightning.utilities import rank_zero_only

# local modules
import pulsenetlib.model


def get_file_from_regex(save_dir, regex):
    available_files = glob.glob(f'{save_dir}/{regex}')
    # sort available files
    if len(available_files) >= 1:
        available_files.sort(key=os.path.getmtime)
        filename = available_files[-1] 
    else:
        raise FileNotFoundError(
            f'No file with regex ({regex}) found in {save_dir}')
        
    return os.path.join(save_dir,filename)
    

    
def resume_configuration_files(config_dir,
             train_config_path=None,
             model_config_path=None):
    """ Function for overwriting training and model configuration dicts.
    
    Reads the yaml configuration files located in the path indicated in
    config_dir and loads the selected training and model configuration 
    dictionaries.

    ----------    
    ARGUMENTS

        config_dir: folder where the configuration files are located
        train_config_path: training configuration files filename. Optional,
                           if None, default names is used:
                           *_conf.yaml'                        
        model_config_path: model configuration files filename. Optional,
                           if None, default names is used:
                           *_mconf.yaml'

    ----------    
    RETURNS

        training_configuration: Dictionary with training configuration 
        model_configuration: Dictionary with model configuration
    
    """
    if train_config_path is None:
        regex = '*_conf.yaml'
        train_config_path = get_file_from_regex(config_dir, regex) 
    elif not os.path.isfile(train_config_path):
        raise FileNotFoundError(f'{train_config_path} does not exist')
    else:
        pass

    if model_config_path is None:
        regex = '*_mconf.yaml'
        model_config_path = get_file_from_regex(config_dir, regex) 
    elif not os.path.isfile(model_config_path):
        raise FileNotFoundError(f'{model_config_path} does not exist')
    else:
        pass
        
    # reading the yaml files and updating the dictionaries
    with open(train_config_path, 'r') as f:
        train_configuration = yaml.load(f, Loader=yaml.FullLoader)
    
    with open(model_config_path, 'r') as f:
        model_configuration = yaml.load(f, Loader=yaml.FullLoader)

    return train_configuration, model_configuration



def load_model_spec(model_dir, model_file_path=None):
    """ Function for loading a predefined model.
    
    Reads the file located in the path indicated in model_dir.
    It loads the version of the module that define the model present
    in its folder.

    ----------    
    ARGUMENTS
        model_dir: folder where the model was saved
        model_file_path: string with the name of the path to the
                         model module. Optional, if None, files with
                         the for *_model.py are searched and the most
                         recent one is loaded.

    ----------    
    RETURNS
        module_spec: ModuleSpec instance with the module defining 
                     the neural network architecture of the 
                     loaded model
    
    """
    
    # loading and copying the model script (model.py)
    if model_file_path is None:
        regex = '*_model.py'
        model_file_path = get_file_from_regex(model_dir, regex)
    
    if not os.path.isfile(model_file_path):
        raise FileNotFoundError(
            f'At resume, {model_file_path} does not exist'
        )
    
    print(f'Loading corresponding module:\n'
          f'  {model_file_path}')
    
    module_spec = importlib.util.spec_from_file_location(
        'orig_model', model_file_path)
    
    return module_spec



def load_pl_ckpt(ckpt_dir, file_to_load=None):
    """ Function for loading a Lightning checkpoint.
    
    ----------    
    ARGUMENTS
    
        train_configuration: dictionary with the training
                             configuration
        file_to_load: string with the name of the *.pth to
                      be loaded. Optional, if None, the latest
                      *.pth file in folder is selected.

    ----------    
    RETURNS
        path_to_ckpt: /path/to/ligtning/checkpoint.ckpt 
    
    """
    
    if file_to_load is None:        
        regex = '*.ckpt'
        path_to_ckpt = get_file_from_regex(ckpt_dir, regex)
    else:
        path_to_ckpt = os.path.join(ckpt_dir, file_to_load)
    
    if not os.path.isfile(path_to_ckpt):
        raise FileNotFoundError(
            f'Checkpoint file {path_to_ckpt} not found'
       ) 
    else:
        print(f'Loading checkpoint file {path_to_ckpt}\n')
    
    return path_to_ckpt



def save_configuration_files(save_dir,
                training_configuration, model_configuration,
                train_config_path=None, model_config_path=None):
    """ Function to save yaml files with the configuration dictionaries.

    ----------
    ARGUMENTS
    
        training_configuration: training configuration dictionary
        model_configuration: model configuration dictionary
        train_config_path: training configuration files filename. Optional,
                        if None, default names is used:
                        [modelFilename]_conf.yaml'                        
        model_config_path: model configuration files filename. Optional,
                         if None, default names is used:
                         [modelFilename]_mconf.yaml'
        
    ----------
    RETURNS
    
        ##none##
    
    """    
    
    if train_config_path is None:
        train_config_path = os.path.join(
            save_dir,
            training_configuration['modelFilename'] + '_conf.yaml'
        )
            
    if model_config_path is None:
        model_config_path = os.path.join(
            save_dir,
            training_configuration['modelFilename'] + '_mconf.yaml'
        )
    
    print(f'Saving yaml files with configuration dictionaries to folder:'
          f'{save_dir}')
    
    for name, file, config in zip(
                      ['Training','Model'],
                      [train_config_path,model_config_path],
                      [training_configuration,model_configuration]):
        if os.path.isfile(file):
            print(f'  {name} configuration yaml file is being overwriten:\n'
                  f'  {file}')
            
        # create YAML files in output folder (human readable)
        with open(file, 'w') as outfile:
            yaml.dump(config, outfile)

    print('')



@rank_zero_only
def save_model(model_dir, model_name=None):
    """ Function to copy the model.py file into the destination folder.

    ----------
    ARGUMENTS
    
        model_dir: path to save directory where model.py is saved
        model_name: string with the name of the file to be put on
                    the destination folder. Optional, if None,
                    _model.py is used
                  
    ----------
    RETURNS
    
        ##none##
        
    """
    
    model_dir = os.path.realpath(model_dir)

    # get pulsenet library root dir
    library_dir = os.path.join(
                    os.sep,*(
                        os.path.realpath(__file__).split(glob.os.sep)[:-1]
                        )
                    )
    
    if model_name is None:
        model_name = '_model.py'
    else:
        model_name = '_{:}_model.py'.format(model_name)
        
    destination_path = os.path.join(model_dir, model_name)
    
    # overwriting previous file, if present
    if os.path.exists(destination_path):
        print('Model file already present in folder, being overwritten...')
    origin_path = os.path.join(library_dir, 'model.py')
    shutil.copyfile(origin_path, destination_path)
            
    print(f'Copying the model.py to the destination folder:\n' 
          f'  {origin_path}\n'
          f'   to\n'
          f'  {destination_path}\n')



def load_model(model_dir, run_device=None):
    """ Load model and state from files in folder
    
    TODO: merge this with the load_checkpoint function, since they
    are pretty much the same function!

    ----------    
    ARGUMENTS
        
        model_dir: path to the model training files
        run_device: running device. Optional, default is to check and 
                select GPU if available.
    
    ----------    
    RETURNS
        
        net: neural network, updated to the checkpoint on ckpt_path
        state_dict: model's state dict
        train_configuration: dictionary with the training configuration
        model_configuration: dictionary with the model configuration
    
    """

    best_models_path = os.path.join(model_dir, 'checkpoints/best_k_models.yaml')
    with open(best_models_path, 'r') as f:
        temp = yaml.load(f, Loader=yaml.FullLoader)
        key_min = min(temp.keys(), key=(lambda k: temp[k]))
        ckpt_format = os.path.split(key_min)[-1]

    config_path, model_config_path, path_to_ckpt = \
        get_default_files(model_dir, f'checkpoints/{ckpt_format}')

    print(f'Loading checkpoint at {path_to_ckpt}')
    if run_device is None:
        if torch.cuda.is_available():
            run_device = torch.device('cuda:0')
        else:
            run_device = torch.device('cpu')
    
    checkpoint = torch.load(
        path_to_ckpt, map_location=run_device)
   
    # reading the yaml files of the configuration dictionaries
    train_configuration = {}
    model_configuration = {}
    with open(config_path, 'r') as f:
        temp = yaml.load(f, Loader=yaml.FullLoader)
        train_configuration.update(temp)    
    with open(model_config_path, 'r') as f:
        temp = yaml.load(f, Loader=yaml.FullLoader)
        model_configuration.update(temp)
    
    # model properties
    model_name = model_configuration['model']
    channels = model_configuration['channels']
    number_channels = len(channels)

    output_channels = model_configuration['outputChannels']
    for channel in output_channels:
        if channel not in channels:
            raise ValueError(f"Output channel name ('{channel}') "
                             f"not found among available "
                             f"channels ({channels}).")

    number_output_channels = len(output_channels)
    
    input_frames = model_configuration['numInputFrames']
    output_frames = model_configuration['numOutputFrames']

    normalization = model_configuration['normalization']
    spatial_context_name = model_configuration['spatialContext']
    padding_mode = model_configuration['paddingMode']

    std_norm = torch.tensor(
        model_configuration['stdNorm']).to(device=run_device)
    avg_remove = torch.tensor(
        model_configuration['avgRemove']).to(device=run_device)
    
    spatial_context_channel = None
    if spatial_context_name is not None:
        spatial_context_channel = []  
        for index_s, context_name in enumerate(spatial_context_name): 
            for index_c, field in enumerate(channels):
                if field == context_name:
                    spatial_context_channel.append(index_c)
            if spatial_context_channel is None:
                raise ValueError(f"context channel name ('{context_name}') "
                                 f"not found among available "
                                 f"channels ({channels}).")  
    # network
    net = pulsenetlib.model.PulseNet(number_channels,
                                 input_frames,
                                 output_frames,
                                 std_norm,
                                 avg_remove,
                                 num_output_channels=number_output_channels,
                                 normalization=normalization,
                                 model_name=model_name,
                                 spatial_context_channel=spatial_context_channel,
                                 spatial_context_names=spatial_context_name,
                                 padding_mode=padding_mode)
    
    # create a stat_dict with renamed keys (only network, not model)
    state_dict = dict()
    for key in checkpoint['state_dict'].keys():
        state_dict[key.replace('model.','')] = \
            checkpoint['state_dict'][key]
    net.load_state_dict(state_dict)

    return net, state_dict, train_configuration, model_configuration



def get_default_files(model_dir,
                      ckpt_format='./checkpoints/epoch=*.ckpt'):
    """ For a given folder, returns the paths to model training files.
    
    Its is based on the files default names and locations.
    
    ----------
    ARGUMENTS
    
        model_dir: path to folder
        ckpt_format: 
    
    ----------    
    RETURNS
    
        model_path: path of the model .py script
        config_path: path of the training configuration .yaml file
        mconfig_path: path of the model configuration .yaml file
        ckpt_path: path of the model configuration .ckpt file. If more
                   than one file is available on folder, the one that
                   was modified last is selected
    
    """
    paths = []
    for regex in ['*_conf.yaml',
                  '*_mconf.yaml', ckpt_format]:
        paths.append(
            get_file(os.path.join(model_dir, regex))
            )
    config_path, mconfig_path, ckpt_path = paths

    return config_path, mconfig_path, ckpt_path



def get_file(file_path):
    """ Return full path to a regex path.
    If multiple exist, return the youngest one.
    """
    files = glob.glob(file_path)
    if len(files) == 0: raise FileNotFoundError(f'{file_path} not found.')
    elif len(files) > 1: files.sort(key=os.path.getmtime)
    return files[-1]



def save_VTK_2D(file_name, scalar, scalar_name, dx=1.0, dy=1.0):
    """ Function to save VTK file with a scalar
    
    The array is defined with x in rows and y in columns.
    Files can be read using pulsenetlib.preprocess.VTKLoader class.
    
    ----------
    ARGUMENTS
    
        file_name: path to the vtk file to be generated
        scalar: numpy 2D array with the scalar field
        scalar_name: string with the names
        dx, dy: grid spqcing in x and y. Optional, default is 1.0
        
    ----------
    RETURNS
    
        ##none##
    
    """

    nx, ny = scalar.shape

    image_data = vtk.vtkImageData()
    image_data.SetDimensions(nx,ny,1)
    image_data.SetOrigin(0.0,0.0,0.0)
    image_data.SetSpacing(dx,dy,0.0)
    image_data.AllocateScalars(vtk.VTK_DOUBLE, 1);

    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                image_data.SetScalarComponentFromDouble(
                    i, j, k, 0, scalar[i,j])

    image_data.GetPointData().GetScalars().SetName(scalar_name)

    vtk_writer = vtk.vtkXMLImageDataWriter()
    vtk_writer.SetFileName(file_name)
    vtk_writer.SetInputData(image_data)
    vtk_writer.Write()

if __name__ == "__main___":
    pass
