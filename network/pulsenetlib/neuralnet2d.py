#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
3 levels multiscale network

Inputs are shape (batch, channels, in_dim, height, width),
outputs are shape (batch, channels, out_dim, height, width).
Every channel is a physical quantity (pressure, velocity-x, ...)
and every dimension is a frame in time.

The number of input (data) channels and the number of output
(target) channels are selected at model creation.

"""

# native modules
##none##

# third-party modules
import torch
import torch.nn as nn
import torch.nn.functional as F

# local modules
##none##


class _ConvBlock1(nn.Module):
    """ First block - quarter scale.
    
    Four Conv2d layers, all with kernel_size 3 and padding of 1 (padding
    ensures output size is same as input size)
    
    Optional dropout before final Conv2d layer
    
    ReLU after first two Conv2d layers, not after last two - predictions
    can be +ve or -ve
    
    """
    def __init__(self, in_channels, mid1_channels, mid2_channels,
                 out_channels, padding_mode='zeros', dropout=False):
        super(_ConvBlock1, self).__init__()
        layers = [
            nn.Conv2d(in_channels, mid1_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid1_channels, mid2_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid2_channels, mid1_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
        ]
        
        if dropout:
            layers.append(nn.Dropout())
        
        layers.append(
            nn.Conv2d(
                mid1_channels, out_channels, kernel_size=3, padding=1, padding_mode=padding_mode)
                     )
        
        self.encode = nn.Sequential(*layers)

    def forward(self, x):
        return self.encode(x)

class _ConvBlock2(nn.Module):
    """ Second block - half scale.
    
    Six Conv2d layers. First one kernel size 5, padding 2, remainder
    kernel size 3 padding 1.
    
    Optional dropout before final Conv2d layer
    
    ReLU after first four Conv2d layers, not after last two - predictions
    can be +ve or -ve
    
    """
    def __init__(self, in_channels, mid1_channels, mid2_channels, mid3_channels,
                 out_channels, padding_mode='zeros', dropout=False):
        super(_ConvBlock2, self).__init__()
        layers = [
            nn.Conv2d(in_channels, mid1_channels, kernel_size=5, padding=2, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid1_channels, mid2_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid2_channels, mid3_channels,kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid3_channels, mid2_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid2_channels, mid1_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
        ]
        
        if dropout:
            layers.append(nn.Dropout())
        
        layers.append(
            nn.Conv2d(
                mid1_channels, out_channels, kernel_size=3, padding=1, padding_mode=padding_mode)
                      )
        
        self.encode = nn.Sequential(*layers)

    def forward(self, x):
        return self.encode(x)

class _ConvBlock3(nn.Module):
    """ Third block - full scale.
    
    Six Conv2d layers. First and last kernel size 5, padding 2, remainder
    kernel size 3 padding 1.
    
    Optional dropout before final Conv2d layer
    
    ReLU after first four Conv2d layers, not after last two - predictions
    can be +ve or -ve
    
    """
    def __init__(self, in_channels, mid1_channels, mid2_channels, mid3_channels,
                 out_channels, padding_mode='zeros', dropout=False):
        super(_ConvBlock3, self).__init__()
        layers = [
            nn.Conv2d(in_channels, mid1_channels, kernel_size=5, padding=2, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid1_channels, mid2_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid2_channels, mid3_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid3_channels, mid2_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid2_channels, mid1_channels, kernel_size=3, padding=1, padding_mode=padding_mode),
        ]
        
        if dropout:
            layers.append(nn.Dropout())
        
        layers.append(
            nn.Conv2d(
                mid1_channels, out_channels, kernel_size=5, padding=2, padding_mode=padding_mode)
                     )
        
        self.encode = nn.Sequential(*layers)

    def forward(self, x):
        return self.encode(x)

    
    
class ImplicitBCMultiScaleNet(nn.Module):
    """ Defines the neural network. Treats BC in an implicit way
     (padding, spatial context).
    
    Input when called is number of input and output channels.
    
        - Downsample input to quarter scale and use ConvBlock1.
        - Upsample output of ConvBlock1 to half scale.
        - Downsample input to half scale, concat to output of ConvBlock1;
          use ConvBLock2.
        - Upsample output of ConvBlock2 to full scale.
        - Concat input to output of ConvBlock2, use ConvBlock3. Output of
          ConvBlock3 has 8 channels
        - Use final Conv2d layer with kernel size of 1 to go from 8
          channels to output channels.
         
    """
    def __init__(self, data_channels, out_channels=1, padding_mode='zeros', dropout=False):
        
        super(ImplicitBCMultiScaleNet, self).__init__()
        
        self.convN_4 = _ConvBlock1(data_channels,
                                   32, 64, out_channels,
                                   padding_mode, dropout=dropout)
        self.convN_2 = _ConvBlock2(data_channels + out_channels,
                                   32, 64, 128, out_channels,
                                   padding_mode, dropout=dropout)
        self.convN_1 = _ConvBlock3(data_channels + out_channels,
                                   32, 64, 128, 8,
                                   padding_mode, dropout=dropout)
        
        self.final = nn.Conv2d(8, out_channels, kernel_size=1)

    def forward(self, x):
        
        quarter_size = [int(i*0.25) for i in list(x.size()[2:])]
        half_size = [int(i*0.5) for i in list(x.size()[2:])]
        
        convN_4out = self.convN_4(
            F.interpolate(x,(quarter_size),
                          mode="bilinear",
                          align_corners=True)
                                 )
        
        convN_2out = self.convN_2(
            torch.cat((F.interpolate(x,(half_size),
                                     mode="bilinear",
                                     align_corners=True),
                       F.interpolate(convN_4out,(half_size),
                                     mode="bilinear",
                                     align_corners=True)),
                      dim=1)
                                 )
        
        convN_1out = self.convN_1(
            torch.cat((F.interpolate(x,(x.size()[2:]),
                                     mode="bilinear",
                                     align_corners=True),
                       F.interpolate(convN_2out,(x.size()[2:]),
                                     mode="bilinear",
                                     align_corners=True)),
                      dim=1)
                                 )
        
        final_out = self.final(convN_1out)
        
        return final_out

class ExplicitBCMultiScaleNet(nn.Module):
    """ Defines the neural network. Treats BC in an explicit way
     (fix values at output).

    Works only for absorbing or reflecting cases.
    
    Input when called is number of input and output channels.
    
        - Downsample input to quarter scale and use ConvBlock1.
        - Upsample output of ConvBlock1 to half scale.
        - Downsample input to half scale, concat to output of ConvBlock1;
          use ConvBLock2.
        - Upsample output of ConvBlock2 to full scale.
        - Concat input to output of ConvBlock2, use ConvBlock3. Output of
          ConvBlock3 has 8 channels
        - Use final Conv2d layer with kernel size of 1 to go from 8
          channels to output channels.
         
    """
    def __init__(self, data_channels, out_channels=1, padding_mode='zeros', dropout=False):
        
        super(ExplicitBCMultiScaleNet, self).__init__()
        
        self.convN_4 = _ConvBlock1(data_channels,
                                   32, 64, out_channels,
                                   padding_mode, dropout=dropout)
        self.convN_2 = _ConvBlock2(data_channels + out_channels,
                                   32, 64, 128, out_channels,
                                   padding_mode, dropout=dropout)
        self.convN_1 = _ConvBlock3(data_channels + out_channels,
                                   32, 64, 128, 8,
                                   padding_mode, dropout=dropout)
        
        self.final = nn.Conv2d(8, out_channels, kernel_size=1)

        self.outpad = nn.ConstantPad2d(1, 0)

    def forward(self, x, mask_absorbing=None, mask_reflecting=None, mask_periodic=None):
        bsz = x.size(0)
        nx = x.size(-1)
        ny = x.size(-2)
        device = x.device
        # border ids
        # 1: absorbing
        # 2: reflecting
        borders = {'left': torch.zeros((bsz,), dtype=torch.long, device=device),
                   'right': torch.zeros((bsz,), dtype=torch.long, device=device),
                   'bottom': torch.zeros((bsz,), dtype=torch.long, device=device),
                   'top':torch.zeros((bsz,), dtype=torch.long, device=device)}
        is_absorbing = False
        is_reflecting = False
        if mask_absorbing is not None:
            p_n = x[:,-1].unsqueeze(1).clone()
            is_absorbing = True
            #left
            borders['left'] = borders['left'].masked_fill_(mask_absorbing[:,ny//2,0] == 1, 1)
            #right
            borders['right'] = borders['right'].masked_fill_(mask_absorbing[:,ny//2,-1] == 1, 1)
            #bottom
            borders['bottom'] = borders['bottom'].masked_fill_(mask_absorbing[:,0,nx//2] == 1, 1)
            #top
            borders['top'] = borders['top'].masked_fill_(mask_absorbing[:,-1,nx//2] == 1, 1)
        if mask_reflecting is not None:
            is_reflecting = True

            borders['left'] = borders['left'].masked_fill_(mask_reflecting[:,ny//2,0] == 1, 2)
            #right
            borders['right'] = borders['right'].masked_fill_(mask_reflecting[:,ny//2,-1] == 1, 2)
            #bottom
            borders['bottom'] = borders['bottom'].masked_fill_(mask_reflecting[:,0,nx//2] == 1, 2)
            #top
            borders['top'] = borders['top'].masked_fill_(mask_reflecting[:,-1,nx//2] == 1, 2)

        quarter_size = [int(i*0.25) for i in list(x.size()[2:])]
        half_size = [int(i*0.5) for i in list(x.size()[2:])]
        convN_4out = self.convN_4(
            F.interpolate(x,(quarter_size),
                          mode="bilinear",
                          align_corners=False)
                                 )
        
        convN_2out = self.convN_2(
            torch.cat((F.interpolate(x,(half_size),
                                     mode="bilinear",
                                     align_corners=False),
                       F.interpolate(convN_4out,(half_size),
                                     mode="bilinear",
                                     align_corners=False)),
                      dim=1)
                                 )
        
        convN_1out = self.convN_1(
            torch.cat((F.interpolate(x,(x.size()[2:]),
                                     mode="bilinear",
                                     align_corners=False),
                       F.interpolate(convN_2out,(x.size()[2:]),
                                     mode="bilinear",
                                     align_corners=False)),
                      dim=1)
                                 )
        
        out =self.final(convN_1out)

        #absorbing
        if is_absorbing:
            # LODI hypothesis with 1st order Finite Differences (FD) in time and in space
            # WARNING: This is only valid for a time-step of DtNN = DtLB because of the
            # Courant number (CFL), change it accordingly if you modify the NN input time sampling.
            CFL = 1/3**0.5 * 1.0
            out[borders['left']==1,0,:,0] = \
                    (p_n[borders['left']==1,0,:,0] + CFL*out[borders['left']==1,0,:,1]) / (1+CFL)
            out[borders['left']==1,0,:,0] = \
                    (p_n[borders['right']==1,0,:,-1] + CFL*out[borders['right']==1,0,:,-2]) / (1+CFL)
            out[borders['bottom']==1,0,0,:] = \
                    (p_n[borders['bottom']==1,0,0,:] + CFL*out[borders['bottom']==1,0,1,:]) / (1+CFL)
            out[borders['top']==1,0,-1,:] = \
                    (p_n[borders['top']==1,0,-1,:] + CFL*out[borders['top']==1,0,-2,:]) / (1+CFL)
        
        #reflecting
        if is_reflecting:
            # 1st order Finite Differences (FD) in space
            out[borders['left']==2,0,:,0] = \
                    out[borders['left']==2,0,:,1]
            out[borders['right']==2,0,:,-1] = \
                    out[borders['right']==2,0,:,-2]
            out[borders['bottom']==2,0,0,:] = \
                    out[borders['bottom']==2,0,1,:]
            out[borders['top']==2,0,-1,:] = \
                    out[borders['top']==2,0,-2,:]
        
        return out
