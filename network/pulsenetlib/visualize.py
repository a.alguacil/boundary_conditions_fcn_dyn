#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions for plotting the estimated fields and associated errors

"""


# native modules
import glob
import copy
import warnings

# third-party modules
import torch
import numpy as np
import scipy.interpolate
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import matplotlib.colors
import matplotlib.patches as patches
from pytorch_lightning.loggers import TensorBoardLogger

# local modules
import pulsenetlib.tools
import pulsenetlib.logger


class TrainingPanel(object):
    r""" Plot a panel with fields and slices of output and target.
    
    For plotting the predicted and target fields and evaluating
    the quality of the model
    
    ----------
    ARGUMENTS
    
        output: tensor with the model output (predicted) field,
                format (H, W)
        target: tensor with the target field, format (H, W)
        slice_dict: dictionary with the slice to plot properties
                    {'x_slice' or 'y_slice' : location}. Optional,
                    if None, no slices are plotted.
        epoch: integer with the number of the epoch.
               Optional, None as default.
        field_name: string with the field name.
                    Optional; "" as default
        normalization: normalization to be perfomed on the fields.
                       Optional, None as default.
        plotField: boolean for indicating to plot the fields.
                   Optional, True as default
        plotFieldGrad: boolean for indicating to plot the fields
                       gradients. Optional, True as default.
        title: string with the title of the figure.
               Optional, True as default.
    
    ----------
    RETURNS

        ##none##
    
    """
    def __init__(self,
                output, target,
                slice_dict=None,
                epoch=None,
                field_name="",
                normalization=None,
                plotField=True,
                plotFieldGrad=True,
                title=None):
                        
        self.epoch = epoch

        matplotlib.rc('text')
    
        ############ general plot properties ############
        self.colorbar_label_format = '%.1e'
        self.colorbar_label_size = 10
        self.title_fontsize = 15
        self.label_fontsize = 15
    
        # colors of the line for the slices plot
        slice_output_color = "C0"
        slice_target_color = "C1"
    
        # scalar and error fields colormaps. Set pixel to black if 
        # value is 'nan' or 'inf'
        target_cmap = copy.copy(cm.get_cmap('seismic'))
        target_cmap.set_bad(color='black')

        prediction_cmap = copy.copy(cm.get_cmap('seismic'))
        prediction_cmap.set_bad(color='black')

        err_cmap = copy.copy(cm.OrRd)
        err_cmap.set_bad(color='black')
    
        # figure properties
        px, py = 790, 475
        dpi = 50
        # figure size (in inches)
        figx, figy = px/dpi, py/dpi
        # field dimension (x in cols, y in rows)
        nx, ny = list(target.shape)[-2:]

        # initiating properties
        plotSlices = False
        self.x_slice = None
        self.y_slice = None

        if slice_dict is not None:
            if len(slice_dict.keys()) != 1:
                raise ValueError('Only one slice allowed in `slice_dict`')
            plotSlices = True

            selected_slice = list(slice_dict.keys())[0]
            slice_position = slice_dict[selected_slice]

            if selected_slice == 'x_slice': self.x_slice = slice_position            
            elif selected_slice == 'y_slice': self.y_slice = slice_position
            else: raise ValueError('Type must be either x_slice or y_slice')
            
        fieldnorm = 1.0
        gradnorm = 1.0
    
        if normalization is not None:
            fieldnorm = normalization[0]
            gradnorm = normalization[1]

        self.formatter = ticker.ScalarFormatter(
            useOffset=True, useMathText=True)
        self.formatter.set_powerlimits((-2,2))
    
        if plotFieldGrad:
            gdl_tar = torch.zeros(1, 2, ny, nx)
            gdl_out = torch.zeros(1, 2, ny, nx)

            # function to select range of plot
            select_range = lambda field: \
                np.max(
                    np.abs(
                        [fun(np.ma.masked_invalid(field))
                            for fun in (np.min, np.max)]
                            )
                    )

            # adapting the prediction and target to the shape necessary
            # for calculating the derivatives (N, C, T, H, W)
            target_to_der = target.unsqueeze(0).unsqueeze(0)
            output_to_der = output.unsqueeze(0).unsqueeze(0)

            # defines a mask based on the nan values
            # if nan's are created during the training, (should not
            # be happening) they will also be ignored when calculating
            # the derivatives and plotting!
            mask = torch.isnan(target).squeeze()

            # Building the derivation operator. Since the derivation algorithm
            # considers a different standard (True for calculating the 
            # derivation and False for ignoring), the opposite of mask is used
            derivate = pulsenetlib.tools.Derivation2D(
                nx, ny, 1, mask=~mask, device=target.device)

            gdl_tar[:,0], gdl_tar[:,1] = derivate(target_to_der)
            gdl_out[:,0], gdl_out[:,1] = derivate(output_to_der)

            gdl_tar = gdl_tar/gradnorm
            gdl_out = gdl_out/gradnorm

            # transforming into numpy arrays
            gdl_tar_x = torch.squeeze(gdl_tar[:,0]).data.numpy()
            gdl_tar_y = torch.squeeze(gdl_tar[:,1]).data.numpy()

            gdl_out_x = torch.squeeze(gdl_out[:,0]).data.numpy()
            gdl_out_y = torch.squeeze(gdl_out[:,1]).data.numpy()

            err_grad_x = np.abs(gdl_out_x - gdl_tar_x)
            err_grad_y = np.abs(gdl_out_y - gdl_tar_y)

            max_err_gradx = np.max(np.ma.masked_invalid(err_grad_x))
            max_err_grady = np.max(np.ma.masked_invalid(err_grad_y))
            # common max - for colorbar
            max_err_grad = np.max([max_err_gradx, max_err_grady])

            # MaxMin grad-x and grad-y
            mmax_gradx_tar = select_range(gdl_tar_x)
            mmax_grady_tar = select_range(gdl_tar_y)

            max_gradx, min_gradx = 1.2*mmax_gradx_tar, -1.2*mmax_gradx_tar
            max_grady, min_grady = 1.2*mmax_grady_tar, -1.2*mmax_grady_tar

        if plotField or plotSlices:
            tar_np = torch.squeeze(target / fieldnorm).data.numpy()
            out_np = torch.squeeze(output / fieldnorm).data.numpy()
            err = (output / fieldnorm - target / fieldnorm)
            err_np = np.abs(torch.squeeze(err).data.numpy())

            mmax_tar = select_range(tar_np)
            max_val_tar, min_val_tar = 1.2*mmax_tar, -1.2*mmax_tar

        if plotSlices:
            # defining slices
            s = np.linspace(0, nx)
            if self.x_slice is not None: f_s = self.x_slice
            if self.y_slice is not None: f_s = self.y_slice

        ncol = 3
        if plotSlices: ncol += 1
        width_ratios = ncol*[1]        

        nrow = 0
        if plotField: nrow += 1
        if plotFieldGrad: nrow += 2
        height_ratios = nrow*[1]

        # starting figure
        self.fig = plt.figure(figsize=(figx, figy), dpi=dpi)

        # defining subplot grid    
        self.gridspec = gridspec.GridSpec(nrow, ncol,
                                          width_ratios=width_ratios,
                                          height_ratios=height_ratios,
                                          wspace=0.2, hspace=0.2,
                                          top=0.91, bottom=0.05,
                                          left=0.05, right=0.95)

        if title is not None:
            if self.epoch is not None:
                self.fig.suptitle(r'$\bf{PulseNet\ output}$' + \
                             ' - Loss = ' + title + \
                             ' - ' + r'$\bf{' + 'Epoch : ' + str(self.epoch) + '}$')
            else:
                self.fig.suptitle(r'$\bf{PulseNet\ output}$' ' - Loss = ' + title)


        # TODO: replace the multiple calls to plot_slice and plot_field by
        # a loop and create a dict with the recurrent parameters (title, colormap, etc)
        it_row = 0
        # plotting scalar field
        if plotField:        
            it_col = 0
            if plotSlices:
                if self.x_slice is not None:
                    line_out = out_np[f_s,:]
                    line_tar = tar_np[f_s,:]
                if self.y_slice is not None:
                    line_out = out_np[:,f_s]
                    line_tar = tar_np[:,f_s]

                mean_y = 0 

                # plotting slices
                self.plot_slice(it_row, it_col,
                                line_out, line_tar, mean_y,
                                axis_title=field_name,
                                axis_label=field_name,
                                color_out=slice_output_color,
                                color_tar=slice_target_color)
                it_col += 1

            # plotting the predicted field
            self.plot_field(it_row, it_col,
                            out_np,
                            axis_title='prediction',
                            colormap=prediction_cmap,
                            colorbar_limits=[min_val_tar,max_val_tar],
                            s=s, f_s=f_s,
                            slice_color=slice_output_color)        
            it_col += 1

            # plotting the target field
            self.plot_field(it_row, it_col,
                            tar_np,
                            axis_title='target',
                            colormap=target_cmap,
                            colorbar_limits=[min_val_tar,max_val_tar],
                            s=s, f_s=f_s,
                            slice_color=slice_target_color)        
            it_col += 1

            # plotting the error field
            self.plot_field(it_row, it_col,
                            err_np,
                            axis_title='PixelSquareError',
                            colormap=err_cmap,
                            colorbar_limits=None,
                            slice_color=None)        
            it_row += 1

        # plotting gradient
        if plotFieldGrad:
            it_col = 0
            if plotSlices:
                # plotting slices of the gradient field
                if self.x_slice is not None:
                    grad_out = gdl_out_x[f_s,:]
                    grad_tar = gdl_tar_x[f_s,:]
                if self.y_slice is not None:
                    grad_out = gdl_out_x[:,f_s]
                    grad_tar = gdl_tar_x[:,f_s]

                mean_y = 0 #np.mean(grad_tar)
                # plotting slices
                self.plot_slice(it_row, it_col,
                                grad_out, grad_tar, mean_y,
                                axis_title=None,
                                axis_label='x-gradient',
                                color_out=slice_output_color,
                                color_tar=slice_target_color)
                it_col += 1

            # plotting the predicted x-gradient
            self.plot_field(it_row, it_col,
                            gdl_out_x,
                            axis_title=None,
                            colormap=prediction_cmap,
                            colorbar_limits=[min_gradx,max_gradx],
                            s=s, f_s=f_s,
                            slice_color=slice_output_color)
            it_col += 1

            # plotting the target x-gradient
            self.plot_field(it_row, it_col,
                            gdl_tar_x,
                            axis_title=None,
                            colormap=target_cmap,
                            colorbar_limits=[min_gradx,max_gradx],
                            s=s, f_s=f_s,
                            slice_color=slice_target_color)
            it_col += 1
            
            # plotting the x-gradient error
            self.plot_field(it_row, it_col,
                            err_grad_x,
                            axis_title=None,
                            colormap=err_cmap,
                            colorbar_limits=[0,max_err_grad],
                            slice_color=None)
            it_row += 1

            it_col = 0
            #x_slice = 90
            if plotSlices:
                if self.x_slice is not None:
                    grad_out = gdl_out_y[f_s,:]
                    grad_tar = gdl_tar_y[f_s,:]
                if self.y_slice is not None:
                    grad_out = gdl_out_y[:,f_s]
                    grad_tar = gdl_tar_y[:,f_s]

                mean_y = 0 # np.mean(grad_tar)            
                self.plot_slice(it_row, it_col,
                                grad_out, grad_tar, mean_y,
                                axis_title=None,
                                axis_label="y-gradient",
                                color_out=slice_output_color,
                                color_tar=slice_target_color)
                it_col += 1

            # plotting the predicted y-gradient
            self.plot_field(it_row, it_col,
                            gdl_out_y,
                            axis_title=None,
                            colormap=prediction_cmap,
                            colorbar_limits=[min_grady,max_grady],
                            s=s, f_s=f_s,
                            slice_color=slice_output_color)
            it_col += 1

            # plotting the target y-gradient
            self.plot_field(it_row, it_col,
                            gdl_tar_y,
                            axis_title=None,
                            colormap=target_cmap,
                            colorbar_limits=[min_grady,max_grady],
                            s=s, f_s=f_s,
                            slice_color=slice_target_color)
            it_col += 1

            # plotting the y-gradient error
            self.plot_field(it_row, it_col,
                            err_grad_y,
                            axis_title=None,
                            colormap=err_cmap,
                            colorbar_limits=[0,max_err_grad],
                            s=s, f_s=f_s,
                            slice_color=None)
            it_row += 1


    def log_figure(self, logger, tag):
        if isinstance(logger, TensorBoardLogger):
            import io
            import PIL.Image
            import torchvision.transforms

            buf = io.BytesIO()
            self.fig.savefig(buf, format='png')
            buf.seek(0)
            image = PIL.Image.open(buf)
            image = torchvision.transforms.ToTensor()(image)

            print('Logging: {:}'.format(tag))
            logger.experiment.add_image(tag, image,
                                        global_step=self.epoch,
                                        dataformats='CHW')
    
        if isinstance(logger, pulsenetlib.logger.DiskLogger):
            print('Logging: {:}'.format(tag))
            logger.experiment.log_images(tag, self.fig, step=self.epoch)


    def save_figure(self, filename):
        print('Saving: {:}'.format(filename))
        self.fig.savefig(filename)

    
    def close_figure(self):
        plt.close(self.fig)


    # Functions defining the plots (field and slice)    
    def plot_field(self,
                   it_row, it_col, field,
                   axis_title,
                   colormap, colorbar_limits,
                   s=None, f_s=None,
                   slice_color=None):
        """ Function to plot a scalar field in the subplot grid

        -----------
        ARGUMENTS

            it_row, it_col: index of the row and column on the
                            subplot grid
            field: 2D array with the scalar field
            axis_title: string with the axis title.
                        None for no title.
            colormap: plot colormap
            colobar_limits: tuple with colorbar limits
            slice_color: color of the line indicating the slice.
                         Optional, none (no plot) as default.

        ----------
        RETURNS

            ##none##

        """

        # selecting the axis in the grid
        ax = plt.subplot(self.gridspec[it_row, it_col])

        if axis_title is not None:
            ax.set_title(axis_title, fontsize=self.title_fontsize)

        # creating axis for the colorbar
        cax = make_axes_locatable(ax).append_axes(
            "right",size="5%",pad="2%")

        # clearing axis labels
        #ax.axis("off") # remove everything
        ax.set_xticklabels([]) # remove only ticks labels
        ax.set_yticklabels([])

        # plotting the field
        # if the colorbar limits are not defined, the default
        if colorbar_limits is None:
            im = ax.imshow(field,
                           cmap=colormap,
                           origin="lower",
                           interpolation="none")
        else:
            im = ax.imshow(field,
                           cmap=colormap,
                           origin="lower",
                           interpolation="none",
                           clim=colorbar_limits)

        # adding the colorbar
        cbar = self.fig.colorbar(im,
                            cax=cax,
                            format=self.colorbar_label_format)
        cbar.ax.tick_params(labelsize=self.colorbar_label_size)

        # fixing the axis limits based on the field
        xlim, ylim = ax.get_xlim(), ax.get_ylim()

        if slice_color is not None:
            # adding lines indicating the slices
            if self.x_slice is not None:
                ax.plot(s, [f_s for i in range(len(s))], c=slice_color)
            if self.y_slice is not None:
                ax.plot([f_s for i in range(len(s))], s, c=slice_color)

        # re-setting to original axis limits
        ax.set_xlim(xlim), ax.set_ylim(ylim)
    
    def plot_slice(self,
                   it_row, it_col,
                   line_out, line_tar, mean_value,
                   axis_title=None, axis_label=None,
                   color_out=None, color_tar=None):
        """ Function to plot a slice (lines) in the subplot grid

        ----------
        ARGUMENTS

            it_row, it_col: index of the row and column on
                            the subplot grid
            line_out: output values for the slice
            line_tar: target values for the slice
            mean_value: value to be removed for both output
                        and target
            axis_title: string with the axis title.
                        Optional, None (no title) as default
            axis_label: string with the y-axis label.
                        Optional, None (no label) as default

        ----------
        RETURNS

            ##none##

        """

        ax = plt.subplot(self.gridspec[it_row,it_col])

        ax.plot(line_out - mean_value,
                label="output", c=color_out)
        ax.plot(line_tar - mean_value,
                label="target", c=color_tar)

        ax.yaxis.set_major_formatter(self.formatter)

        x0, x1 = ax.get_xlim()
        y0, y1 = ax.get_ylim()
        ax.set_aspect((x1 - x0)/(y1 - y0))

        ax.legend()

        if axis_title is not None:
            ax.set_title(axis_title, fontsize=self.title_fontsize)
        if axis_label is not None:
            ax.set_ylabel(axis_label, fontsize=self.label_fontsize)



def plot_loss(train_file_name, valid_file_name, figure_path):
    r"""
    Plot loss evolution for training and validation
    
    ----------
    ARGUMENTS
    
        train_file_name:
        valid_file_name:
        figure_path:
        
    ----------
    RETURNS
    
        ##none##
    
    """
    
    #TODO make it more universal in the case of multiple loss terms
    
    # Load numpy array
    #file_train = glob.os.path.join(d, 'train_loss.npy')
    
    
    # properties for each curve ()
    cases_to_plot = ([train_file_name,
                      {'label':'training',
                       'linestyle':'-',
                       'linewidth':1.2,
                       'color':'sienna',
                       'marker':'^',
                       'markersize':0.0,
                       'markevery':10,
                       'markeredgecolor':'k',
                       'alpha':1,
                      }
                     ],
                     [valid_file_name,
                      {'label':'validation',
                       'linestyle':':',
                       'linewidth':1.5,
                       'color':'k',
                       'marker':'s',
                       'markersize':0,
                       'markevery':10,
                       'markeredgecolor':'k',
                       'alpha':1,
                      }
                     ])
    
    fig, ax = plt.subplots(figsize=(5,5))
    
    for file, plot_dict in cases_to_plot:
        loss_array = np.load(file)        
        num_losses = len(loss_array[0])-1
        
        epoch = loss_array[:,0]
        loss = loss_array[:,1:]
                
        print('File : {:} has {:d} losses'.format(
            file,num_losses))
        ax.semilogy(epoch[:], loss[:,-1],
                    **plot_dict)
    
    ax.set_xlabel('epoch')
    ax.set_ylabel('loss')
    ax.legend()
    ax.grid(True, which='both',
            linewidth=.5, alpha=.5, color='gray')
    
    plt.tight_layout()
    plt.savefig(figure_path)



def plot_loss_npz(metrics_file_name, figure_path):
    r"""
    Plot loss evolution for training and validation
    
    ----------
    ARGUMENTS
    
        metrics_file_name:
        figure_path:
        
    ----------
    RETURNS
    
        ##none##
    
    """
    
    #TODO make it more universal in the case of multiple loss terms
    
    # Load numpy array
    #file_train = glob.os.path.join(d, 'train_loss.npy')
    
    
    # properties for each curve ()
    losses = np.load(metrics_file_name)    
    files = losses.files  
    
    epochs = losses['epoch']
    cases_to_plot = ([losses['loss'],
                      {'label':'training',
                       'linestyle':'-',
                       'linewidth':1.2,
                       'color':'sienna',
                       'marker':'^',
                       'markersize':0.0,
                       'markevery':10,
                       'markeredgecolor':'k',
                       'alpha':1,
                      }
                     ],
                     [losses['val_loss'],
                      {'label':'validation',
                       'linestyle':':',
                       'linewidth':1.5,
                       'color':'k',
                       'marker':'s',
                       'markersize':0,
                       'markevery':10,
                       'markeredgecolor':'k',
                       'alpha':1,
                      }
                     ])
    
    fig, ax = plt.subplots(figsize=(5,5))
    
    for loss, plot_dict in cases_to_plot:
        ax.semilogy(epochs, loss, 
                    **plot_dict)
    
    ax.set_xlabel('epoch')
    ax.set_ylabel('loss')
    ax.legend()
    ax.grid(True, which='both',
            linewidth=.5, alpha=.5, color='gray')
    
    plt.tight_layout()
    plt.savefig(figure_path)
    
    
    
def plot_learning_rate(lr_file_name, figure_path):
    """ Plot learning rate evolution.
    #TODO: complete this
    ----------
    ARGUMENTS
    
        lr_file_name:
        figure_path:
        
    ----------    
    RETURNS
    
        ##none##
    
    """
    
    # starting figure
    fig, ax = plt.subplots(figsize=(5,5))
    
    array = np.load(lr_file_name)
    
    epoch = array[:,0]
    lr = array[:,1:]
    
    plot_dict = {'label':'learning rate',
                 'linestyle':'-',
                 'linewidth':1.5,
                 'color':'k',
                 'marker':'s',
                 'markersize':0,
                 'markevery':10,
                 'markeredgecolor':'k',
                 'alpha':1,
                 }
        
    ax.semilogy(epoch,lr,**plot_dict)

    ax.set_xlabel('epoch')
    ax.set_ylabel('learning rate')
    ax.grid(True, which='both',
            linewidth=.5, alpha=.5, color='gray')

    plt.tight_layout()
    plt.savefig(figure_path)

def slice2DField(x, y, field, x0, y0, x1, y1, num_points=100):
    """ Slice a 2D field for any direction.

    ----------
    ARGUMENTS:
        x: x-coordinates of field
        y: y-coordinates of field
        field: 2D field
        x0, y0: coordinates of slice starting point
        x1, y1: coordinates of slice ending point
        num_points: number of points used for interpolation

    --------
    RETURNS:
        profile: sliced values along the specified line

    """
    xvalues = np.linspace(x0, x1, num_points)
    yvalues = np.linspace(y0, y1, num_points)
    f = scipy.interpolate.interp2d(x, y, field)

    # Extract the values along the line
    profile = np.zeros(num_points)
    for s in range(num_points):
        profile[s] = f(xvalues[s], yvalues[s])
    return profile
 

class RecursivePanelPlot():
    """ Panel with the time evolution of 2D fields.

    Default inputs will produce a plot with the first row as the
    reference (simulated data), second row for estimations and third
    row for error.
    
    A path is added at the start of the plot such as to hide
    frames not issued from a run of the model.
    
    TODO: Add an axes for the colorbar!

    ----------
    ARGUMENTS

        nx, ny: field height and width
        num_input_frames: number of input frames
        num_output_frames: number of output frames
        num_rows: number of quantities (rows) to be plotted. Optional, 
                  default is 3 (reference, estimation and square error)  
        title_fun: function that returns an string used as row title based
                   on the plot index (from 0 to the number of total frames).
                   Normally associated with the number of the frames or
                   timestep. Optional, default returns an empty title
    ----------
    RETURNS
    
        ##none##

    """
    
    def default_title(self,args):
        return ''

    def __init__(self, nx, ny, num_input_frames, num_output_frames,
                 title_fun=None,
                 num_rows=3,
                 colormaps=('seismic','seismic','hot'),
                 limits=([-8e-4, 8e-4],[-8e-4, 8e-4],[1e-10,1]),
                 labels=('$\\mathrm{target}$\n$f(t)$',
                         '$\\mathrm{estimation}$\n$\\tilde{f}(t)$',
                         r'$\dfrac{[f(t) - \tilde{f}(t)]^2}{\mathrm{max}(f(t))}$'),
                 log_colorbar=(False,False,True),
                 cover_row=(False,True,True),
                ):
        
        if title_fun is None:
            self.title_fun = self.default_title
        else:
            self.title_fun = title_fun 
        
       
        self.input_frames = num_input_frames
        self.cover_row = cover_row
        self.log_colorbar = log_colorbar
        
        total_number_frames = num_input_frames + num_output_frames
        
        self.fig, self.axs = plt.subplots(
            num_rows, total_number_frames + 1,
            gridspec_kw={'width_ratios': [*([1]*total_number_frames), .25],
                         'height_ratios': [1]*num_rows},
            figsize=(total_number_frames, num_rows),
            dpi=150)

        dummy_data = np.zeros((nx, ny))
        self.plot_objs = np.zeros(self.axs.shape,dtype=object)
        self.plot_patches = self.plot_objs.copy()

        # colorbar axis
        self.caxs = np.zeros((num_rows),dtype=object)
        self.cbars = self.caxs.copy()

        # setting format of axis labels and starting plots
        for index_row, cmap, limit, label in zip(
                                range(num_rows), colormaps, limits, labels):

            for index_col in range(total_number_frames):
                ax = self.axs[index_row, index_col]
                # remove only ticks labels
                ax.set_xticklabels([]), ax.set_yticklabels([])
                # remove ticks
                ax.set_xticks([]), ax.set_yticks([])            
                for axis in ['top','bottom','left','right']:
                    ax.spines[axis].set_linewidth(0.25)

                # starting plots
                self.plot_objs[index_row, index_col] = \
                            ax.imshow(dummy_data,
                                      origin='lower', interpolation='none',
                                      cmap=cmap, vmin=limit[0], vmax=limit[1])

            # colorbar on last axes of row
            self.caxs[index_row] = self.axs[index_row, -1]
            self.cbars[index_row] = self.fig.colorbar(
                                        self.plot_objs[index_row, -2],
                                        cax=self.caxs[index_row],
                                        orientation='vertical',
                                        shrink=.5,
                                        #extend='both'
            )
            self.caxs[index_row].set_aspect(10)
            self.cbars[index_row].ax.tick_params(labelsize=5) 
            for axis in ['top','bottom','left','right']:
                self.caxs[index_row].spines[axis].set_linewidth(0.25)

            # add a label at the left of the first field
            self.axs[index_row, 0].annotate(label,
                                       xy=(int(-.2*nx), ny/2),
                                       horizontalalignment='center',
                                       verticalalignment='center',
                                       annotation_clip=False,
                                       rotation=90,
                                       fontsize=7)

        # covering non-pertinent fields. make sure the box color is
        # not present in the colormap to avoid confusion and to close
        # the patches later. Patches are created for all fields, but
        # removed for quantities that are not hidden
        for index_row, cover in enumerate(cover_row):
            for index_col in range(num_input_frames):            
                self.plot_patches[index_row, index_col] = \
                    self.axs[index_row, index_col].add_patch(
                        patches.Rectangle((0, 0), nx, ny, facecolor='gray')
                    )
                if not cover:
                    self.plot_patches[index_row, index_col].remove()

        # logarithmic colormap for the error plot
        for index_row, is_log in enumerate(log_colorbar):
            if is_log:
                for index_col in range(total_number_frames):
                    self.plot_objs[index_row, index_col].set_norm(
                        matplotlib.colors.LogNorm(
                            vmin=limits[index_row][0],
                            vmax=limits[index_row][1],
                            clip=True)
                    )

        def remove_last(array):
            """ Function to remove last element of numpy array.
            One must create a new array because numpy arrays are immutable.
            """
            array_temp = np.zeros(
                (array.shape[0], array.shape[1]-1), dtype=object)
            for index_row in range(array.shape[0]):
                array_temp[index_row,:] = array[index_row,:-1]
            return array_temp
        
        self.axs = remove_last(self.axs)
        self.plot_objs = remove_last(self.plot_objs)

        plt.tight_layout(pad=1, h_pad=0, w_pad=0)
        self.fig.subplots_adjust(wspace=0.1, hspace=0.1)


        
    def update(self, cases, counter):
        """ Update the plotted fields
        
        ----------
        ARGUMENTS
        
            cases: tuple with the (N, H, W) tensors with the values
                   to be plotted on each row
            counter: integer indicating the number of recurrencies.
                     Used to remove the patches hiding non-pertinent
                     fields
        
        ----------
        RETURNS
        
            ##none##
        
        """
        
        for index_row, case in enumerate(cases):
            for index_plot, field in enumerate(case):
                plot_obj = self.plot_objs[index_row, index_plot]
                data_to_plot = field.data.numpy()
                
                # to avoid misconceptions and errors with matplotlib,
                # forcing null values to the limit of colorbar
                # in case of a log scale
                if self.log_colorbar[index_row]:
                    data_to_plot[data_to_plot <= 0] = 1e-30      
                plot_obj.set_data(data_to_plot)
                
                # add title on first row
                if index_row == 0:
                    self.axs[0, index_plot].set_title(
                        self.title_fun(index_plot),
                        fontdict={'fontsize':8})

        # removing patches covering non-pertinent fields, that is,
        # before any estimation
        if counter < self.input_frames + 1 and counter > 0:
            index_to_remove = self.input_frames - counter
            for index_row, cover in enumerate(self.cover_row):
                if cover:
                    self.plot_patches[index_row, index_to_remove].remove()

            

    def save(self, filepath):
        try:
           plt.draw()
        except ValueError:
           # Due to an unkwon behavior, in the case of values increasing
           # and starting to be out of the LogNorm range (but still positive),
           # the imshow plot returns an error:
           #
           # >>> ValueError: minvalue must be positive
           #
           # Current workaround consists in change the range of the plot.
           # This is not a reasonable solution once the generated images
           # will not be directly comparable to the previous plots and may
           # cause confusion.
           # 
           # The operation is performed for all the rows with log scales, even
           # if they do not present the error.
           #
           # TODO: find a better solution and/or report the bug to matplotlib
           # devellopers
           range_factor = 1e3
           for images, is_log in zip(self.plot_objs, self.log_colorbar):
               for image in images:
                  if is_log:
                      vmin, vmax = image.get_clim()
                      warnings.warn('Updating the range of the image due to LogNorm bug')
                      image.set_norm(
                          matplotlib.colors.LogNorm(
                              vmin=vmin*range_factor, vmax=vmax*range_factor)
                      )
           plt.draw()
           
        self.fig.savefig(filepath)
