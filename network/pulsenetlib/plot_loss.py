# native modules
import argparse
import os
import glob

# third party modules
import numpy as np
import matplotlib.pyplot as plt

# local modules
# none

def plot_loss_npz(model_folder, figure_name):
    r"""
    Plot loss evolution for training and validation
    
    ----------
    ARGUMENTS
    
        metrics_file_name:
        figure_path:
        
    ----------
    RETURNS
    
        ##none##
    
    """
    
    #TODO make it more universal in the case of multiple loss terms
    
    # Load numpy array
    #file_train = glob.os.path.join(d, 'train_loss.npy')
    
    metrics_file_name = glob.glob(os.path.join(model_folder, '*.npz'))[0]
    figure_path = os.path.join(model_folder, figure_name)
    
    # properties for each curve ()
    losses = np.load(metrics_file_name, allow_pickle=True)    
    
    files = losses.files  
    epochs = losses['epoch'][::2]
    cases_to_plot = ([losses['loss'],
                      {'label':'training',
                       'linestyle':'-',
                       'linewidth':1.2,
                       'color':'sienna',
                       'marker':'^',
                       'markersize':0.0,
                       'markevery':10,
                       'markeredgecolor':'k',
                       'alpha':1,
                      }
                     ],
                     [losses['val_loss'],
                      {'label':'validation',
                       'linestyle':':',
                       'linewidth':1.5,
                       'color':'k',
                       'marker':'s',
                       'markersize':0,
                       'markevery':10,
                       'markeredgecolor':'k',
                       'alpha':1,
                      }
                     ])
    
    fig, ax = plt.subplots(figsize=(5,5))
    
    for loss, plot_dict in cases_to_plot:
        ax.semilogy(loss, 
                    **plot_dict)
    
    ax.set_xlabel('epoch')
    ax.set_ylabel('loss')
    ax.legend()
    ax.grid(True, which='both',
            linewidth=.5, alpha=.5, color='gray')
    
    plt.tight_layout()
    plt.savefig(figure_path)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot loss script')
    parser.add_argument('--model_folder', '-f',
                         help='Path to the *.npz metrics filename')
    parser.add_argument('--figname', '-n',
                        default='loss.png')

    arguments = parser.parse_args()

    plot_loss_npz(arguments.model_folder, arguments.figname)


