#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Classes that define the loss criterion used for training the
model.

"""

# native modules
import functools

# third-party modules
import torch
import torch.nn as nn

# local modules
import pulsenetlib


class Criterion(nn.Module):    
    def __init__(self, channels, L2lambda, GDL2lambda, acEnergyL2lambda):
        """ Class that defines the loss functions.

        For instanciating, one most indicates the gain (lambda) for
        each component (L2, GDL2 and acEnergyL2Lambda, representing
        the L2-norm, L2-norm of the gradient and L2-norm of the
        acoustic energy respectively). If any of the lambdas is null, the
        corresponding loss is not calculated.

        TODO: make it channel independent (for example, 0.5 for P,
        0.25 for Ux and 0.25 for Uy

        ----------
        ARGUMENTS

            L2lambda: lambda of the L2-norm
            GDL2Lambda: lambda of gradient L2-norm
            acEnergyL2Lambda: lambda of acoustic energy L2-norm.
                              For using this term, all 3 channels
                              (p, Ux and Uy) must be available. Also,
                              the function to calculate the acoustic
                              energy (in acoustics.py) must be adapted
                              if different conditions are in use
        
        ----------
        RETURNS
        
            ##none##
            
        """
                
        super(Criterion, self).__init__()

        # Earlier versions of the code did not have 'channels' as input
        # and had 'run_device'. Reorganizing inputs to guarante backwards
        # compatibility when working with previously trained models. Missing
        # or lost values are implied!
        if isinstance(acEnergyL2lambda, torch.device):
            acEnergyL2lambda = 0.0
            GDL2lambda = L2lambda
            L2lambda = channels
            channels = ['pressure']
            
        # loss types
        self._mse = nn.MSELoss()
        
        # define the functions that calculate the loss forms
        # only selected if the multiplier is bigger than 0
        self.loss_funs = []
        self.loss_names = []

        self.register_buffer('L2lambda', torch.tensor(L2lambda), persistent=False)
        self.register_buffer('GDL2lambda', torch.tensor(GDL2lambda), persistent=False)
        self.register_buffer('acEnergyL2lambda', torch.tensor(acEnergyL2lambda), persistent=False)
                
        if torch.any(self.L2lambda > 0.0):
            for field in channels: 
                self.loss_names.append(f'L2Lambda_{field}')
            self.L2lambda = self.L2lambda.view(1, len(channels), 1, 1, 1)
            self.loss_funs.append(
                self.loss_L2
                             )
                
        if torch.any(self.GDL2lambda > 0.0): 
            for field in channels: 
                self.loss_names.append(f'GDL2Lambda_{field}')   
            self.GDL2lambda = self.GDL2lambda.view(1, len(channels), 1, 1, 1)
            self.loss_funs.append(
                self.loss_GDL2
                             )
            
        if torch.any(self.acEnergyL2lambda > 0.0): 
            for field in channels: 
                self.loss_names.append(f'acEnergyL2Lambda_{field}')   
            self.acEnergyL2lambda = self.acEnergyL2lambda.view(1, len(channels), 1, 1, 1)
            self.loss_funs.append(
                self.loss_energyL2
                                 )
        
        # error when no loss term has been defined
        if len(self.loss_funs) == 0:
            raise ValueError('At least one non-null loss term is necessary.')

        # Normalization by the number of loss components
        #self.normalization =
        self.register_buffer('normalization', torch.ones(len(self)), persistent=False)


    def get_loss_names(self):
        return self.loss_names 
    

    def __len__(self):
        """ Returns the number of loss terms.
        """
        return len(self.loss_funs)
    

    def summary(self):
        """ Prints the loss terms functions.
        """
        print('Selected loss terms:\n')
        for loss_fun in self.loss_funs:
            print(loss_fun,'\n')
    
        
    def forward(self, out, target):
        """ Calculate the loss.

        ----------
        ARGUMENTS
        
            out: model prediction tensor, format (N,C,D,H,W)
            target: target tensor, format (N,C,D,H,W)
            normalization: normalization factor
        
        ----------    
        RETURNS
        
            result: loss tensor, normalized by the
                    normalization factor
        
        """
                
        _assert_no_grad(target)

        if self.normalization.size(0) != len(self):
            raise ValueError(
                'Mismatch between size of normalization ({})\
                and number of losses ({}).'.format(
                    self.normalization.size(0),
                    len(self.loss))
            )

        nx, ny = target.shape[-2:]
        
        # Building the derivation operator.
        number_frames = int(target.numel()/(nx*ny))
        self.derivate = pulsenetlib.tools.Derivation2D(
            nx, ny, number_frames, device=target.device)

        # calculate the loss term for every available function
        result = [loss_fun(out, target)/norm for loss_fun, norm in 
                zip(self.loss_funs, self.normalization)]

        return result
    
        
    def loss_L2(self, prediction, target):
        """ Calculate the loss of the fields
        
        Each frame is normalized by the standard deviation
        observed for each channel in the target's first time 
        frame (:,:,0,:,:).
        
        ----------
        ARGUMENTS
        
            prediction: tensor with the model's output, format
                        (N, C, D, H, W)
            target: tensor with the target, format 
                    (N, C, D, H, W)
            lamb: scalar per channel that indicates the gain of the term in
                  the total loss calculation
        
        ----------
        RETURNS
            
            (loss)
            
        """
        
        std_mse = pulsenetlib.transforms.std_first_frame(target)

        return self._mse(
            prediction/std_mse, target/std_mse)*self.L2lambda
        
    
    def loss_GDL2(self, prediction, target, reduction='mean'):
        """ Calculate the loss of the gradients
        
        Deals with a single channel.
        
        ----------
        ARGUMENTS
        
            prediction: tensor with the model's prediction,
                        format (N, C, D, H, W)
            target: tensor with the target, format
                    (N, C, D, H, W)
            lamb: scalar per channel that indicates the gain of the term in
                  the total loss calculation
            
        ----------
        RETURNS
            
            (loss)
            
        """
        grad_x_tar, grad_y_tar = self.derivate(target)
        
        std_grad_x_first_frame = \
            pulsenetlib.transforms.std_first_frame(grad_x_tar)
        std_grad_y_first_frame = \
            pulsenetlib.transforms.std_first_frame(grad_y_tar)
        
        # selecting the max grad std for each channel for 
        # normalization
        std_grad = \
            torch.max(std_grad_x_first_frame, std_grad_y_first_frame)
        
        grad_x_tar, grad_y_tar = self.derivate(target/std_grad)
        grad_x_pred, grad_y_pred = self.derivate(prediction/std_grad)

        loss = torch.abs(grad_x_pred - grad_x_tar)**2.
        loss += torch.abs(grad_y_pred - grad_y_tar)**2.
    
        if reduction != 'none':
            loss = torch.mean(loss) if reduction == 'mean' \
                else torch.sum(loss)

        return loss*self.GDL2lambda


    
    def loss_energyL2(self, prediction, target):
        """ Calculate the loss of the acoustic energy
        
        For the calculation, the pressure and velocities must
        be present in the tensor (3 channels). For performance,
        the size is not checked!
        
        Attention: for quiescent fluids, reflecting walls, etc
        
        ----------
        ARGUMENTS
        
            prediction: tensor with the model's prediction,
                        format (N, C, D, H, W)
            target: tensor with the target, format
                    (N, C, D, H, W)
            lamb: scalar per channel that indicates the gain of the term in
                  the total loss calculation
                  
        ----------
        RETURNS
            
            (loss)
            
        """
        
        prediction_energy = pulsenetlib.acoustics.acousticEnergy(
            prediction[:,0,:,:,:], prediction[:,1:2,:,:,:])
        target_energy = pulsenetlib.acoustics.acousticEnergy(
            target[:,0,:,:,:], target[:,1:2,:,:,:])
        
        return self._mse(prediction_energy, target_energy)*self.acEnergyL2lambda

    
    
def _assert_no_grad(tensor):
    assert not tensor.requires_grad, \
        "nn criterions don't compute the gradient w.r.t. targets - please " \
        "mark these tensors as not requiring gradients"
