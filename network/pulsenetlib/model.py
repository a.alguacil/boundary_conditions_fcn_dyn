#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Definition of the neural network model
"""

# native modules
##none##

# third-party modules
import torch
import torch.nn as nn

# local modules
import pulsenetlib


class PulseNet(nn.Module):
    """ Class of the neural network model
    
    Currently, only considers the 2D convolution network.
    
    
    ----------    
    ARGUMENTS
    
        number_channels: number of channels that are considered
                         by the model (data fields)
        input_frames: number of frames of the input (number of 
                      frames)
        output_frames: number of frames of the output/target                 
        std_norm: tensor with the normalization factors (must have
                  as much values as channels - second dimension
                  of the input)        
        avg_remove: tensor with the average removal factors (must
                    have as much values as channels - second dimension
                    of the input)       
        model_name: architecture name (avaliable 'ScaleNet', 'GeneralScaleNet',
                    'ScaleNet3D')
        scales_description: list with description for Multiscale model
        padding_mode: padding mode for model       
        dropout: boolean for setting dropout technique.
                 Optional, default is True
        
    """
    def __init__(self, number_channels, input_frames,
                 output_frames, std_norm, avg_remove,
                 num_output_channels=None,
                 model_name='implicitBC',
                 spatial_context_channel=None,
                 spatial_context_names=None,
                 normalization='gaussian',
                 padding_mode='zeros'):
        super(PulseNet, self).__init__()
        
        # training parameters
        self.spatial_context_channel = spatial_context_channel
        self.num_spatial_context = 0
        if spatial_context_channel is not None:
            self.num_spatial_context = len(spatial_context_channel)
        self.spatial_context_names = spatial_context_names
        self.num_input_channels = number_channels
        self.num_output_channels = number_channels
        if num_output_channels is not None:
            self.num_output_channels = num_output_channels
        self.num_output_frames = output_frames
        self.num_pages = input_frames*(self.num_input_channels
                 - self.num_spatial_context)
        self.num_pages_with_context = self.num_pages + self.num_spatial_context
        if self.num_pages < 1:
            raise ValueError('num_pages found to be negative or zero.')

        self.register_buffer('std_norm', std_norm, persistent=False)
        self.register_buffer('avg_remove', avg_remove, persistent=False)
        self.normalization = normalization
        if not isinstance(self.normalization , list):
            self.normalization = [normalization]*self.num_input_channels
        else:
            if len(normalization) != (self.num_input_channels):
                raise ValueError(f'normalization must have same length '
                                 f'as number of input channels '
                                 f'({self.num_input_channels}). '
                                 f'Found len {len(normalization)}')

        
        # defining the neural network: MultiScaleNet
        self.explicit_bc = False
        if model_name == 'implicitBC':
            self.net = pulsenetlib.neuralnet2d.ImplicitBCMultiScaleNet(
                            self.num_pages_with_context, self.num_output_channels, padding_mode)
        elif model_name == 'explicitBC':
            self.explicit_bc = True
            if self.spatial_context_names is None:
                raise ValueError(f'spatial_context_names mut be implemented for'
                                 f'model_name=explicitBC')
            self.net = pulsenetlib.neuralnet2d.ExplicitBCMultiScaleNet(
                            self.num_pages, self.num_output_channels, padding_mode)
        else:
            raise ValueError(f'Model name "{model_name}" is not implemented.')
        
        
        
    def forward(self, input_):
        """ Method to call the network and evaluate the model.
        
        Both the inut and the output are in physical units.
        It contains the data treatement (normalization,
        multiplication, etc) for before and after the network.
        
        ----------
        ARGUMENTS
        
            input_: tensor in the format (N, C, T_in, (D), H, W)
                N: number of batch
                C: channel (physical quantity)
                T_in: input time frame
                (D): frame depth (optional for 3d data)
                H: frame height
                W: frame width
        
        ----------
        RETURNS
        
            out: tensor with the prediction, of the same format
                 single frame (N, C, T_out, (D), H, W)
        
        """
        
        # input format
        bsz, csz, tsz = input_.shape[:3]


        dim_shape = ()
        for size in input_.shape[3:]:
            dim_shape = dim_shape + (size,)

        data_channels = torch.arange(self.num_input_channels, dtype=torch.long, device=input_.device) 
        
        # contexts are additional input channels for encoding positional info
        # physical values are well defined (e.g. boundary condition)
        if self.num_spatial_context > 0: 
            context_channels = torch.tensor(
                self.spatial_context_channel,
                dtype=torch.long, device=input_.device
                ) 
            spatial_context = input_.index_select(dim=1, index=context_channels)
            # chose only first step
            first_frame = torch.tensor(0, dtype=torch.long, device=input_.device)
            spatial_context = spatial_context[:,:,0]
            for i in self.spatial_context_channel:
                data_channels = data_channels[data_channels!=i]

        input_ = input_.index_select(dim=1, index=data_channels).contiguous() 

        std_first_frames = []
        mean_first_frames = []
        gaussian_norm_channels = []
        gaussian_norm_counter = 0
        for channel, norm in enumerate(self.normalization):
            if norm == 'gaussian': 
                gaussian_norm_channels.append(gaussian_norm_counter)
                # calculate the std of the first frame (timestep) of every
                # element in the input scene
                first_frame = input_[:,channel,0].clone().unsqueeze(1).unsqueeze(1)

                std_first_frames.append(
                    pulsenetlib.transforms.std_first_frame(first_frame)
                )  
                mean_first_frames.append(
                    pulsenetlib.transforms.mean_first_frame(first_frame)
                )

                # remove the average, considering the first frame
                pulsenetlib.transforms.add_frame_(
                    input_[:,channel], mean_first_frames[gaussian_norm_counter], self.avg_remove[gaussian_norm_counter])
                # normalize by std, considering the first frame
                pulsenetlib.transforms.divide_frame_(
                    input_[:,channel], std_first_frames[gaussian_norm_counter], self.std_norm[gaussian_norm_counter])

                gaussian_norm_counter += 1
        

        # Input dim = (batch, channels, time, ...) 
        # channels and time dimensions are combined in order
        # to perform 2D (BSZ, C, H, W) or
        # 3D (BSZ, C, D, H, W) convolutions.
        # Data is presented to the neural network as:
        #
        #     channel 0: frame 0, channel 1: frame 0, ....
        #       channel 0: frame 1, channel 1, frame 1, ....
        #         channel 0: frame 2, channel 1, frame 2, ....
        #           ...
        #
        # the transpose operation is necessary so they are sorted
        # by time and not by channel
        input_shape = (bsz,self.num_pages) + dim_shape
        x = input_.transpose(1,2).contiguous().view(input_shape)
        

        # calling the network
        if self.explicit_bc:
            model_kwargs = {'mask_absorbing':None, 'mask_reflecting':None, 'mask_periodic':None}
            for i, context in enumerate(self.spatial_context_names):
                 model_kwargs[context] = spatial_context[:,i]
            y = self.net(x, **model_kwargs)
        else:
            # concatenate spatial context at the end of input
            if self.num_spatial_context > 0:
                x = torch.cat((x, spatial_context), dim=1).contiguous()
            y = self.net(x)

        
        # returning to original data format!
        # first as (BSZ, T, C, ...), later modified to (BSZ, C, T, ...)
        # by the transpose method
        output_shape = (bsz, self.num_output_frames, self.num_output_channels) + dim_shape
        out = y.view(output_shape).transpose(1,2)
        
        for channel in gaussian_norm_channels:
            # going back to physical quantity performing the opposite
            # operations
            pulsenetlib.transforms.multiply_frame_(
                out[:,channel], std_first_frames[channel], self.std_norm[channel]
            )
            pulsenetlib.transforms.add_frame_(
                out[:,channel], mean_first_frames[channel], -self.avg_remove[channel]
            )
        
        return out
    
    
