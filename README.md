# Effects of Boundary Conditions in Fully Convolutional Networks for Learning Spatio-temporal Dynamics (ECML-PKDD 2021)

This repository contains the data, code and additional results of our [paper](https://arxiv.org/abs/2106.11160) accepted to the Applied Data Science Tracks at ECML-PKDD 2021. If you find this code useful in your research, please consider citing:
    
    @misc{alguacil2021effects,
      title={Effects of boundary conditions in fully convolutional networks for learning spatio-temporal dynamics}, 
      author={Antonio Alguacil and Wagner Gonçalves Pinto and Michael Bauerheim and Marc C. Jacob and Stéphane Moreau},
      year={2021},
      eprint={2106.11160},
      archivePrefix={arXiv},
      primaryClass={cs.LG}
    }


The repository is organized as follows:

- [data_generation](./data_generation): code for the generation of the database using Palabos
- [network](./network): implementation of the neural network, train and testing scripts

You can browse the different subfolder to generate the data with an open-source CFD code, train the neural network or
test the method.

Network architecture
------------

The employed neural network is a Multi-Scale architecture [from this paper](https://arxiv.org/abs/1511.05440). 3 Scales are used, with dimensions N, N/2 and N/4, composed by 17 two-dimensional convolution operations, for a total of 422,419 trainable parameters. ReLUs are used as activation function and replication padding is used to maintain layers size unchanged inside each scale.

<p align="center">
  <img alt="Neural network architecture" src="./images/drawing_network_architecture.png" width="800"/>
</p>

Computing environment
------------

Hardware and versions of the software used in the study are listed below:

+ CPU: Intel® Xeon® Gold 6126 Processor 2.60 GHz
+ GPU: Nvidia Tesla V100 32Gb, Nvidia GPU Driver Version 455.32
+ Software: [Python](https://www.python.org/) 3.8.5, [PyTorch](https://pytorch.org/) 1.7.0, [pytorch-lightning](https://www.pytorchlightning.ai/) 1.0.1, [numpy](https://numpy.org/) 1.19.2 and [CUDA](https://developer.nvidia.com/cuda-toolkit) 11.1

Explicit BCs enforcing: implementation details
------------

The explicit BCs rules are implemented using Finite Difference (FD) schemes in [network/pulsenetlib/neuralnet2d.py:ExplicitBCMultiScaleNet](./network/pulsenetlib/neuralnet2d.py).

### Reflecting and Adiabatic BCs (Neumann)
The Neumann BCs reads (for either density or temperature fields):

<img src='./images/eqs/eq1.png' alt="1">

Employing a 1st order finite difference spatial scheme, the missing values can be set as:

<img src='./images/eqs/eq2.png' alt="1"> 

and:

<img src='./images/eqs/eq3.png' alt="1"> 

where t represents the temporal dimension and i the spatial dimension.

### Absorbing BCs (LODI Chacteristics)
For the absorbing case, non-reflecting boundary conditions are modeled with the simple
Locally-One Dimensional Inviscid (LODI) hypothesis, written as

<img src='./images/eqs/eq4.png' alt="1"> 

where <img src='./images/eqs/c0.png' alt="1"> denotes the speed of sound.

Employing first order FD schemes in time and space, the missing boundary values can be written as

<img src='./images/eqs/eq5.png' alt="1"> 

and

<img src='./images/eqs/eq6.png' alt="1">

where <img src='./images/eqs/cfl.png' alt="1"> denotes the Courant–Friedrichs–Lewy number, relating the speed of sound,
temporal and space steps in explicit schemes.


Additional Results
------------

## Dataset 1: Reflecting walls

**Initial condition 000, reflecting BCs (dataset D1)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=40</th>
    <th>it=80</th>
    <th>it=120</th>
    <th>it=160</th>
    <th>it=200</th>
    <th>it=240</th>
    <th>it=280</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d1/impl/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/target_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="4"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d1/impl/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d1/impl/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d1/impl/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d1/impl/reflect/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d1/context/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d1/context/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d1/context/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d1/context/reflect/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Explicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d1/explicit/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d1/explicit/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d1/explicit/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d1/explicit/reflect/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
   <tr>
    <td colspan="13" align="center"><img src="./images/d1/impl/replicate/ic0/cbar.png"  alt="1" width = 382px height = 73px > </td> 
   </tr> 
</table>


**Initial condition 014, reflecting BCs (dataset D1)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=60</th>
    <th>it=80</th>
    <th>it=120</th>
    <th>it=160</th>
    <th>it=200</th>
    <th>it=240</th>
    <th>it=280</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d1/impl/replicate/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/target_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="4"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d1/impl/zeros/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/zeros/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d1/impl/circular/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/circular/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d1/impl/replicate/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/replicate/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d1/impl/reflect/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/impl/reflect/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d1/context/zeros/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/zeros/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d1/context/circular/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/circular/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d1/context/replicate/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/replicate/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d1/context/reflect/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/context/reflect/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Explicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d1/explicit/zeros/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/zeros/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d1/explicit/circular/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/circular/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d1/explicit/replicate/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/replicate/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d1/explicit/reflect/ic14/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d1/explicit/reflect/ic14/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
   <tr>
    <td colspan="13" align="center"><img src="./images/d1/impl/replicate/ic14/cbar.png"  alt="1" width = 382px height = 73px > </td> 
   </tr> 
</table>

## Dataset 2: Periodic walls

**Initial condition 000, periodic BCs (dataset D2)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=40</th>
    <th>it=80</th>
    <th>it=120</th>
    <th>it=160</th>
    <th>it=200</th>
    <th>it=240</th>
    <th>it=280</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d2/impl/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/target_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="3"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d2/impl/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d2/impl/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d2/impl/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
    <td rowspan="3"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d2/context/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d2/context/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d2/context/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic0/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
    <td colspan="13" align="center"><img src="./images/d2/impl/replicate/ic0/cbar.png"  alt="1" width = 382px height = 73px > </td> 
   </tr> 
</table>

**Initial condition 001, periodic BCs (dataset D2)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=40</th>
    <th>it=80</th>
    <th>it=120</th>
    <th>it=160</th>
    <th>it=200</th>
    <th>it=240</th>
    <th>it=280</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d2/impl/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/target_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="3"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d2/impl/zeros/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/zeros/ic1/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d2/impl/circular/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/circular/ic1/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d2/impl/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/impl/replicate/ic1/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
    <td rowspan="3"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d2/context/zeros/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/zeros/ic1/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d2/context/circular/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/circular/ic1/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d2/context/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_40.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_80.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_160.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_200.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d2/context/replicate/ic1/output_280.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
    <td colspan="13" align="center"><img src="./images/d2/impl/replicate/ic1/cbar.png"  alt="1" width = 382px height = 73px > </td> 
   </tr> 
</table>

## Dataset 3: Absorbing walls

**Initial condition 000, absorbing BCs (dataset D3)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=60</th>
    <th>it=120</th>
    <th>it=180</th>
    <th>it=240</th>
    <th>it=300</th>
    <th>it=360</th>
    <th>it=420</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d3/impl/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/target_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="4"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d3/impl/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d3/impl/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d3/impl/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d3/impl/reflect/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d3/context/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d3/context/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d3/context/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d3/context/reflect/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Explicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d3/explicit/zeros/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d3/explicit/circular/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d3/explicit/replicate/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d3/explicit/reflect/ic0/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic0/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
   <tr>
    <td colspan="13" align="center"><img src="./images/d3/impl/replicate/ic0/cbar.png"  alt="1" width = 382px height = 73px > </td> 
   </tr> 
</table>

**Initial condition 001, absorbing BCs (dataset D3)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=60</th>
    <th>it=120</th>
    <th>it=180</th>
    <th>it=240</th>
    <th>it=300</th>
    <th>it=360</th>
    <th>it=420</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d3/impl/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/target_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="4"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d3/impl/zeros/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/zeros/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d3/impl/circular/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/circular/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d3/impl/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/replicate/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d3/impl/reflect/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/impl/reflect/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d3/context/zeros/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/zeros/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d3/context/circular/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/circular/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d3/context/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/replicate/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d3/context/reflect/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/context/reflect/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="4"> Explicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d3/explicit/zeros/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/zeros/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Circular</td>
    <td> <img src="./images/d3/explicit/circular/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/circular/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d3/explicit/replicate/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/replicate/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d3/explicit/reflect/ic1/input_0.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_60.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_120.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_180.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_240.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_300.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_360.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d3/explicit/reflect/ic1/output_420.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
   <tr>
    <td colspan="13" align="center"><img src="./images/d3/impl/replicate/ic1/cbar.png"  alt="1" width = 382px height = 73px > </td> 
   </tr> 
</table>

**Initial condition 000, adiabatic BCs (dataset D4)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=12</th>
    <th>it=16</th>
    <th>it=20</th>
    <th>it=12</th>
    <th>it=80</th>
    <th>it=100</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d4/implicit/replicate/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/target_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/target_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/target_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/target_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/target_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/target_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="3"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d4/implicit/zeros/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d4/implicit/replicate/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d4/implicit/reflect/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="3"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d4/context/zeros/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d4/context/replicate/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d4/context/reflect/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="3"> Explicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d4/explicit/zeros/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d4/explicit/replicate/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d4/explicit/reflect/id000/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id000/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id000/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id000/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id000/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id000/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
</table>

**Initial condition 014, adiabatic BCs (dataset D4)**

<table>
  <tr>
    <th>Method</th>
    <th>Padding</th>
    <th>it=0</th>
    <th>it=12</th>
    <th>it=16</th>
    <th>it=20</th>
    <th>it=12</th>
    <th>it=80</th>
    <th>it=100</th>
  </tr>
  <tr>
    <td> Ground truth</td>
    <td> </td>
    <td> <img src="./images/d4/implicit/replicate/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/target_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/target_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/target_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/target_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/target_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/target_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr> 
  <tr>
    <td rowspan="3"> Implicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d4/implicit/zeros/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/zeros/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d4/implicit/replicate/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/replicate/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d4/implicit/reflect/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/implicit/reflect/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="3"> Context</td>
    <td> Zeros</td>
    <td> <img src="./images/d4/context/zeros/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/zeros/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d4/context/replicate/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/replicate/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d4/context/reflect/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/context/reflect/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td rowspan="3"> Explicit</td>
    <td> Zeros</td>
    <td> <img src="./images/d4/explicit/zeros/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/zeros/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Replicate</td>
    <td> <img src="./images/d4/explicit/replicate/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/replicate/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
  <tr>
    <td> Reflect</td>
    <td> <img src="./images/d4/explicit/reflect/id014/input_0.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id014/output_16.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id014/output_20.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id014/output_12.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id014/output_80.svg.png"  alt="1" width = 80px height = 80px ></td>
    <td> <img src="./images/d4/explicit/reflect/id014/output_100.svg.png"  alt="1" width = 80px height = 80px ></td>
   </tr>
</table>
